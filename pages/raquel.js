import Head from 'next/head'
import firebase from "firebase/app";
import "firebase";
var raquel = require('../arquivos/Raquel.json');
var cidades = require('../arquivos/cidades.json');
export default function Home() {
  async function buscaImovel() {
    const firebaseConfig = {
      apiKey: 'AIzaSyCRY1RsUASByrwHaiXSzgLBa_KjcxA7HDk',
      authDomain: 'smartimob-dev-test.firebaseapp.com',
      databaseURL: 'https://smartimob-dev-test.firebaseio.com',
      projectId: 'smartimob-dev-test',
      storageBucket: 'smartimob-dev-test.appspot.com',
      messagingSenderId: '1030189605462',
      appId: '1:1030189605462:web:cb42f70fe0f892048051f7',
      measurementId: 'G-KC82QWH0ZD'
    };
    
    const app = firebase.initializeApp(firebaseConfig);
    app.firestore().settings({ experimentalForceLongPolling: true });
  
    var imovel = [];
    var imagem = [];
    var caracteristicas = [];
    var regioes = [
      {
        id: 35,
        sigla: "SP",
        nome: "São Paulo",
        regiao: {
          id: 3,
          sigla: "SE",
          nome: "Sudeste"
        }
      },
      {
        id: 31,
        sigla: "MG",
        nome: "Minas Gerais",
        regiao: {
          id: 3,
          sigla: "SE",
          nome: "Sudeste"
        }
      }
    ]
    var bairro_disponiveis = [];
    var caracteristicas_disponiveis = [];
    
    for await (let imo of raquel.Imoveis ) {
      let template = {
          caracteristicas: [],
          banheiros: '',
          preço_condominio: 0,
          fotos: [],
          fotos_thumbnail: [],
          CEP_confirmado: true,
          CEP_error: false,
          excluido: false,
          agenciador_id: "",
          codigo: '',
          db_id: '',
          aut_venda: [],
          aut_visita: [],
          cadastrador_id: "",
          campos_personalizados_values: [],
          created_at: new Date(),
          proprietario_id: '',
          bairro: '',
          rua: '',
          número: '',
          estado: {
          id: '',
          nome: '',
          value: ''
          },
          CEP: '',
          cidade: {
          id: '',
          nome: '',
          value: ''
          },
          exibirEndereco: true,
          vagas: '',
          suítes: '',
          dormitórios: '',
          tipo: '',
          aceita_permuta: false,
          mostrar_permuta_no_site: false,
          venda_exibir_valor_no_site: true,
          permuta_desc: "",
          area_total: '',
          area_construída: "",
          area_privativa: "",
          area_útil: "",
          preço_venda: '',
          preço_venda_desconto: 0,
          preço_locação: 0,
          preço_locação_desconto: 0,
          venda_autorizacao: false,
          venda_financiamento: false,
          locação_exibir_valor_no_site: false,
          locação_autorizacao: false,
          bloqueio_fotos: false,
          locação: false,
          venda: true,
          IPTU: 0,
          titulo: "",
          descrição: '',
          desc_interna: "",
          video_youtube: "",
          palavras_chaves: "",
          link_tour_virtual: "",
          inativo: false,
          destaque: ""
      }
      let caracteristica = 
            [   
                {
                    nome: imo['Piscina'] == "1" ? 'Piscina' : 'Piscina',
                    value: imo['Piscina'] == "1" ? true : false,
                },
                {
                    nome: imo['Sauna'] == "1" ? 'Sauna' : 'Sauna',
                    value: imo['Sauna'] == "1" ? true : false
                },
                {
                    nome: imo['Adega'] == "1" ? 'Adega' : 'Adega',
                    value: imo['Adega'] == "1" ? true : false,
                },
                {
                    nome: imo['Terraco'] == "1" ? 'Terraco' : 'Terraco',
                    value: imo['Terraco'] == "1" ? true : false
                },
                {
                    nome: imo['BeiraMar'] == "1" ? 'Beira Mar' : 'Beira Mar',
                    value: imo['BeiraMar'] == "1" ? false : true
                },
                {
                    nome: imo['JardimInverno'] == "1" ? 'Jardim Inverno' : 'Jardim Inverno',
                    value: imo['JardimInverno'] == "1" ? true : false
                },
                {
                    nome: imo['PeDireitoDuplo'] == "1" ? 'PeDireito Duplo' : 'PeDireito Duplo',
                    value: imo['PeDireitoDuplo'] == "1" ? true : false
                },
                {
                    nome: imo['Mezanino'] == "1" ? 'Mezanino' : 'Mezanino',
                    value: imo['Mezanino'] == "1" ? true : false
                },
                {
                    nome: imo['PisoElevado'] == "1" ? 'Piso Elevado' : 'Piso Elevado',
                    value: imo['PisoElevado'] == "1" ? true : false
                },
                {
                    nome: imo['Deposito'] == "1" ? 'Deposito' : 'Deposito',
                    value: imo['Deposito'] == "1" ? true : false
                },
                {
                    nome: imo['Escritorio'] == "1" ? 'Escritorio' : 'Escritorio',
                    value: imo['Escritorio'] == "1" ? true : false
                }
            ]
        let caracteristica_disponiveis = 
        [   
            {
                nome: imo['Piscina'] == "1" ? 'Piscina' : 'Piscina',
                id: imo['Piscina'] == "1" ? 'Piscina' : 'Piscina',
            },
            {
                nome: imo['Sauna'] == "1" ? 'Sauna' : 'Sauna',
                id: imo['Sauna'] == "1" ? 'Sauna' : 'Sauna'
            },
            {
                nome: imo['Adega'] == "1" ? 'Adega' : 'Adega',
                id: imo['Adega'] == "1" ? 'Adega' : 'Adega',
            },
            {
                nome: imo['Terraco'] == "1" ? 'Terraco' : 'Terraco',
                id: imo['Terraco'] == "1" ? 'Terraco' : 'Terraco'
            },
            {
                nome: imo['BeiraMar'] == "1" ? 'Beira Mar' : 'Beira Mar',
                id: imo['BeiraMar'] == "1" ? 'Beira Mar' : 'Beira Mar',
            },
            {
                nome: imo['JardimInverno'] == "1" ? 'Jardim Inverno' : 'Jardim Inverno',
                id: imo['JardimInverno'] == "1" ? 'Jardim Inverno' : 'Jardim Inverno'
            },
            {
                nome: imo['PeDireitoDuplo'] == "1" ? 'Pe Direito Duplo' : 'Pe Direito Duplo',
                id: imo['PeDireitoDuplo'] == "1" ? 'Pe Direito Duplo' : 'Pe Direito Duplo',
            },
            {
                nome: imo['Mezanino'] == "1" ? 'Mezanino' : 'Mezanino',
                id: imo['Mezanino'] == "1" ? 'Mezanino' : 'Mezanino',
            },
            {
                nome: imo['PisoElevado'] == "1" ? 'Piso Elevado' : 'Piso Elevado',
                id: imo['PisoElevado'] == "1" ? 'Piso Elevado' : 'Piso Elevado'
            },
            {
                nome: imo['Deposito'] == "1" ? 'Deposito' : 'Deposito',
                id: imo['Deposito'] == "1" ? 'Deposito' : 'Deposito'
            },
            {
                nome: imo['Escritorio'] == "1" ? 'Escritorio' : 'Escritorio',
                id: imo['Escritorio'] == "1" ? 'Escritorio' : 'Escritorio',
            }
        ]
      var results = []
      var e = 0;
      let imo_import = await firebase.firestore().doc(`empresas/xxLUZju6uieKet0pf3Dp/imoveis/${imo.CodigoImovel}`).get();
      imo_import = await imo_import.data();
      
      if (imo.Fotos && imo.Fotos.length > 0) {
          for await (let imagem of imo.Fotos) {
              var img = [];
              
              let im = await fetch(`http://localhost/test_sockets.php?imagem=${imagem.URLArquivo}`).then( dados => dados.json()).catch(function(error){ return error});
              
              var storageCodigoImovel = firebase.storage().ref();
                       
              var fotoCodigoImovel = storageCodigoImovel.child(`empresas/xxLUZju6uieKet0pf3Dp/${imo.CodigoImovel}/${imo.CodigoImovel}_${e}.jpeg`);
              if (im.caminho) {
                let snapshot = await fotoCodigoImovel.putString(im.caminho, 'data_url',{contentType:'image/jpeg'})
                results.push({
                  destaque: e === 0 ? true : false,
                  height: null,
                  id: `${imo.CodigoImovel}_${e}`,
                  uploaded: true,
                  width: null,
                  resized: await fotoCodigoImovel.getDownloadURL(),
                  source:{uri: await fotoCodigoImovel.getDownloadURL()}
                })
                e++;
                console.log("foto inserida: ",e); 
              }         
          }
      }
      // console.log(regioes.filter( estados => estados.sigla == imo.UF).length > 0 ? regioes.filter( estados => estados.sigla == imo.UF)[0].id : "");
      // return true;
      let cliente = {
            foto: null,
            nome: imo.Proprietario.Nome ? imo.Proprietario.Nome : "",
            email: imo.Proprietario.Email ? imo.Proprietario.Email : "",
            telefone: imo.Proprietario.Telefone ? imo.Proprietario.Telefone : "",
            CPF: null,
            FGTS: null,
            excluido: false,
            dependentes: null,
            entrada: 1,
            imoveis_cadastrados: [],
            imoveis_visitados: [],
            possui_emprestimo: false,
            funil_etapa_venda: 0,
            renda: null,
            proprietario: false,
            status: 'Cadastrado pela landpage',
            troca: false,
            DDD: 1,
            utilizar_financiamento: false,
            created_at: new Date(),
            edited_at: new Date()
        }
        let cli = await firebase.firestore().doc(`empresas/xxLUZju6uieKet0pf3Dp/clientes/${imo.Proprietario.Telefone}`).set({...cliente});
      template = {
        ...template,
        codigo: imo.CodigoImovel,
        tipo: imo.TipoImovel ? imo.TipoImovel : "",
        excluido: imo.Ativo === "1" ? true : false,
        inativo: imo.Publicado != "1" ? false : true,
        cidade: {
            id: cidades.filter( city => city.municipio.nome == imo.Cidade && city.municipio.microrregiao.mesorregiao.UF.sigla == imo.UF).length > 0 ? cidades.filter( city => city.municipio.nome == imo.Cidade && city.municipio.microrregiao.mesorregiao.UF.sigla == imo.UF)[0].municipio.id : "",
            nome: imo.Cidade,
            value: cidades.filter( city => city.municipio.nome == imo.Cidade && city.municipio.microrregiao.mesorregiao.UF.sigla == imo.UF).length > 0 ? cidades.filter( city => city.municipio.nome == imo.Cidade && city.municipio.microrregiao.mesorregiao.UF.sigla == imo.UF)[0].municipio.id : "",
        },
        agenciador_id: 's5zCiF8hRKejMedhLVhT9IYr6JC2',
        bairro: imo.Bairro ? imo.Bairro : "",
        rua: imo.Endereco ? imo.Endereco : "",
        número: imo.Numero ? imo.Numero : "",
        CEP: imo.CEP ? imo.CEP : "",
        vagas: imo.QtdVagas ? imo.QtdVagas : 0,
        area_útil: imo.AreaUtil ? imo.AreaUtil : "",
        dormitórios: imo.QtdDormitorios ? imo.QtdDormitorios : 0,
        suítes: imo.QtdSuites ? imo.QtdSuites : 0,
        banheiros: imo.QtdBanheiros ? imo.QtdBanheiros : 0,
        preço_locação: imo.PrecoLocacao ? imo.PrecoLocacao : "",
        preço_condominio: imo.PrecoCondominio ? imo.PrecoCondominio : "",
        IPTU: imo.ValorIPTU ? imo.ValorIPTU : "",
        caracteristicas: caracteristica,
        estado: {
          id: regioes.filter( estados => estados.sigla == imo.UF).length > 0 ? regioes.filter( estados => estados.sigla == imo.UF)[0].id : "",
          nome: regioes.filter( estados => estados.sigla == imo.UF).length > 0 ? regioes.filter( estados => estados.sigla == imo.UF)[0].nome : "",
          value: regioes.filter( estados => estados.sigla == imo.UF).length > 0 ? regioes.filter( estados => estados.sigla == imo.UF)[0].id : "",
        },
        locação: imo.PrecoLocacao > 0 ? true : false,
        created_at: imo.LastUpdateDate ? imo.LastUpdateDate : new Date(),
        edited_at: imo.LastUpdateDate ? imo.LastUpdateDate : new Date(),
        preço_venda: imo.PrecoVenda ? imo.PrecoVenda : "",
        venda: imo.PrecoVenda > 0 ? true : false,
        descrição: imo.Observacao ? imo.Observacao : "",
        complemento: imo.Complemento ? imo.Observacao : "",
        proprietario_id: imo.Proprietario.Telefone,
        fotos: results,
      }
      if ( bairro_disponiveis.filter( bairro => bairro.bairro == imo.Bairro).length == 0 && imo.Bairro) {
        bairro_disponiveis.push({
          bairro: imo.Bairro,
          cidade: cidades.filter( city => city.municipio.nome == imo.Cidade && city.municipio.microrregiao.mesorregiao.UF.sigla == imo.UF).length > 0 ? cidades.filter( city => city.municipio.nome == imo.Cidade && city.municipio.microrregiao.mesorregiao.UF.sigla == imo.UF)[0].municipio.id : "",
          estado: regioes.filter( estados => estados.sigla == imo.UF).length > 0 ? regioes.filter( estados => estados.sigla == imo.UF)[0].id : "",
        })
      }
      
      
      // if(!imo_import)
        var imovel = firebase.firestore().doc(`empresas/xxLUZju6uieKet0pf3Dp/imoveis/${imo.CodigoImovel}`).set({...template});
      firebase.firestore().doc(`empresas/xxLUZju6uieKet0pf3Dp`).update({bairros_disponiveis: bairro_disponiveis,caracteristicas_disponiveis: caracteristica_disponiveis})
      console.log("inserido: ",imo.CodigoImovel)
      // return true;
      // console.clear();
    }
  }
  buscaImovel();
  return (
    <div>
      tset
    </div>
  )
}
