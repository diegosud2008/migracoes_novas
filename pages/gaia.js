import React, { Component } from 'react';
import firebase from "firebase/app";
import "firebase";

var cidades = require('../arquivos/cidades.json'); // arquivos padrões

// var Usuarios =              require("../Gaia/renatoLins/usuarios.json");
var Clientes =              require("../Gaia/renatoLins/clientes.json");
var ClienteAtendimento =    require('../Gaia/renatoLins/clientesAtendimentos.json');
var ClienteUsuarios =       require("../Gaia/renatoLins/clientesUsuarios.json");
var ClientePerfil =         require('../Gaia/renatoLins/clientesPerfil.json');
var Empreendimento =        require('../Gaia/renatoLins/empreendimentos.json');
// var EmpreendimentoCarac =   require('../Gaia/renatoLins/EmpreendimentosDetalhes.json');
// var EmpreendimentoFotos =   require('../Gaia/renatoLins/empreendimentosFotos.json');
var Imoveis =               require('../Gaia/renatoLins/imoveis.json');
var ImoveisFotos =          require('../Gaia/renatoLins/imoveisFotos.json');

export default class Gaia extends Component {
    state = {
        usuario: "",
        clientes: [],
        imoveis: [],
        empreendimento: [],
        fotos: []
    }
    db = () => {
        const firebaseConfig = {
            apiKey: 'AIzaSyCRY1RsUASByrwHaiXSzgLBa_KjcxA7HDk',
            authDomain: 'smartimob-dev-test.firebaseapp.com',
            databaseURL: 'https://smartimob-dev-test.firebaseio.com',
            projectId: 'smartimob-dev-test',
            storageBucket: 'smartimob-dev-test.appspot.com',
            messagingSenderId: '1030189605462',
            appId: '1:1030189605462:web:cb42f70fe0f892048051f7',
            measurementId: 'G-KC82QWH0ZD'
          };
          
          const app = firebase.initializeApp(firebaseConfig);
          app.firestore().settings({ experimentalForceLongPolling: true });

          return firebase;
    }
    componentDidMount() {
        // this.MigraClientes();
        // this.MigraUser();
        // this.MigraEmpreendimento();
        this.MigraImoveis();
    }
    async MigraUser() {
        let e = 0;
        let db = this.db();
        for await(let user of Usuarios.Usuarios) {
            console.log(user.status);
            if (user.status == "Ativo") {
                let im = await fetch(`http://localhost/test_sockets.php?imagem=${user['url foto']}`).then( dados => dados.json()).catch(function(error){ return error});
                var storageRef = db.storage().ref();
                var fotoRef = storageRef.child(`users/GeOvpcrTxJM0XLknXn3j/img_perfil_${user['nome']}.jpeg`);
                let snapshot = await fotoRef.putString(im.caminho, 'data_url',{contentType:'image/jpeg'})
                let usr = {
                    cargo: user['perfil'] == 'Diretor' ? 'Adiministrador' : 'Corretor',
                    email: user['e-mail'],
                    equipe_id: null,
                    expo_token: [],
                    foto: await fotoRef.getDownloadURL(),
                    imoveis_salvos: [],
                    nome: user['nome'],
                    permissões: {
                        agenda: user['perfil'] == 'Diretor' ? true : false,
                        alterar_cadastro: user['perfil'] == 'Diretor' ? true : false,
                        equipes: user['perfil'] == 'Diretor' ? true : false,
                        exportar_dados: user['perfil'] == 'Diretor' ? true : false,
                        financeiro: user['perfil'] == 'Diretor' ? true : false,
                        imoveis: true,
                        imoveis_criar: true,
                        imoveis_editar: true,
                        marketing: user['perfil'] == 'Diretor' ? true : false,
                        depoimentos: user['perfil'] == 'Diretor' ? true : false,
                        imoveis_ver_tudo: user['perfil'] == 'Diretor' ? true : false,
                        lead_criar: true,
                        lead_editar: true,
                        lead_ver_todos: user['perfil'] == 'Diretor' ? true : false,
                        pipeline_leads: user['perfil'] == 'Diretor' ? true : false
                    },
                    push_web_tokens: [],
                    telefones: user['telefone'],
                    CPF: user['cpf'],
                    CRECI: user['creci'],
                    empresa: 'GeOvpcrTxJM0XLknXn3j',

                }
                let email = user['e-mail'];
                let password = user['e-mail'];
                
                // db.auth().createUserWithEmailAndPassword(email,password);
                console.log(email)
                // let users = db.firestore().collection(`users`).where('email','==',email).get();
                // users = (await users).docs;
                //     users.map( u => {
                //     console.log(usr)
                //     db.firestore().doc(`users/${u.id}`).update({...usr})
                // })
                
                
                this.setState({
                    usuario: user["email"]
                })
            }
            // return true;
        }
    }
    MigraClientes = async () => {
        let db = this.db();
        
        let cliente = {
            foto: null,
            nome: '',
            email: null,
            telefone: null,
            CPF: null,
            FGTS: null,
            excluido: false,
            dependentes: null,
            entrada: 1,
            imoveis_cadastrados: [],
            imoveis_visitados: [],
            possui_emprestimo: false,
            funil_etapa_venda: 0,
            renda: null,
            proprietario: false,
            status: 'Cadastrado pela landpage',
            troca: false,
            DDD: 1,
            utilizar_financiamento: false,
            created_at: new Date(),
            edited_at: new Date()
        }
        for await ( let cli of Clientes.Clientes) {
            let atendimento = ClienteAtendimento.ClientesAtendimentos.filter( aten => aten['id cliente'] == cli['id cliente']);
            let cli_user = ClienteUsuarios.ClientesUsuarios.filter( clie => clie['id cliente'] == cli['id cliente']);
            // let userAtendendo = Usuarios.Usuarios.filter( users => users['id'] == cli_user[cli_user.length - 1]['id usuario']);
            let clienteRef = ClientePerfil.ClientesPerfis.filter( clie => clie['id cliente'] == cli['id cliente'] && clie['referência']);
            
            
            let historico = [];
            // let users = db.firestore().collection(`users`).where('email','==',userAtendendo[0]['e-mail']).get();
            // users = (await users).docs;
            // users = users.map( u => {
            //     return u.id
            // })
            for await ( let aten of atendimento ) {
                historico.push({
                    data: new Date(aten['data cadastro']),
                    descrição: aten['comentario'] ? `${aten['comentario']}. Status: ${aten['status']}` : `status: ${aten['status']}`,
                    titulo: 'Veio da Migração'
                })
            }
            cliente = {
                ...cliente,
                nome: cli['nome'],
                email: cli['emails'] ? cli['emails'] : "",
                telefone: cli['telefones'] ? cli['telefones'].substr(4) : "",
                created_at: new Date(cli['data cadastro']),
                edited_at: new Date(cli['data atualização']),
                excluido: cli['ativo'] == "Não" ? false : true,
                descrição: cli['observações'] ? cli['observações'] : "",
                CPF: cli['cpf'] ? cli['cpf'] : "",
                midia: cli['midia origem'] ? cli['midia origem'] : "",
                timeline: [...historico],
                corretor_responsavel: '26DQH9COCYNtiiOFGWTVHzmSl9o1',
                funil_etapa_venda: 1,
                DDD: cli['telefones'].substr(1, 2),
                codigo_imovel: clienteRef.length > 0 ? clienteRef[0]['referência'] : ''
            }
            console.log(cliente)
            db.firestore().doc(`empresas/GeOvpcrTxJM0XLknXn3j/clientes/${cli['id cliente']}`).set({...cliente})
            
            this.setState({
                clientes: [...this.state.clientes,cliente.nome]
            })
            // return true
        }
    }
    MigraEmpreendimento = async () => {
        let db = this.db();
        for await ( let emp of Empreendimento.Empreendimentos ) {
            let empreend = {
                andares: "",
                caracteristicas: [],
                cep: "",
                cidade_id: "",
                created_at: new Date(),
                edited_at: new Date(),
                estado_id: "",
                finalidade: "",
                fotos: [],
                nome: "",
                numero: "",
                porcentage_vendas_feitas: "",
                preco_condominio: "",
                referencia: "",
                situacao: "",
                unidades_por_andar: "",
                vagas: 0
            }
            let carac = [];

            let empreendCa      = EmpreendimentoCarac.EmpreendimentosDetalhes.filter( empe => empe['id empreendimento'] == emp['id empreendimento']);
            let empreendFotos   = EmpreendimentoFotos.EmpreendimentosFotos.filter( empe => empe['id empreendimento'] == emp['id empreendimento']);

            let fotos = [];
            let e = 0;
            for await ( let empe of empreendFotos ) {
                let im = await fetch(`http://localhost/test_sockets.php?imagem=${empe['url grande']}`).then( dados => dados.json()).catch(function(error){ return error});
              
                var storageRef = db.storage().ref();
                       
                var fotoRef = storageRef.child(`empresas/GeOvpcrTxJM0XLknXn3j/${empe['id empreendimento']}/${empe['id foto']}_${e}.jpeg`);
                if (im.caminho) {
                    let snapshot = await fotoRef.putString(im.caminho, 'data_url',{contentType:'image/jpeg'})
                    fotos.push({
                        destaque: empe['destaque'] == 'Sim' ? true : false,
                        height: null,
                        id: `${empe['id foto']}_${e}`,
                        uploaded: true,
                        width: null,
                        ordem: empe['ordem'],
                        resized: await fotoRef.getDownloadURL(),
                        source:{uri: await fotoRef.getDownloadURL()}
                    })
                    e++;
                    this.setState({
                        fotos: [...this.state.fotos,`Foto inserida: ${empe['id foto']}_${e}`]
                    })
                }
            }


            for await ( let car of empreendCa ) {
                carac.push({
                    id: car['detalhe'],
                    nome: car['detalhe'],
                    value: true
                })
            }
            empreend = {
                ...empreend,
                nome: emp['nome'],
                edíficio: emp['edifício'] == 'Sim' ? true : false,
                situacao: emp['fase'] ? emp['fase'] : "Pronto",
                finalidade: emp['tipo'] == 'Edifício Residencial' ? 'residencial' : 'comercial',
                rua: emp['logradouro'] ? emp['logradouro'] : "",
                cidade_id: cidades.filter( city => city.nome == emp['cidade'] && city.municipio.microrregiao.mesorregiao.UF.sigla == emp['estado']).length > 0 ? cidades.filter( city => city.nome == emp['cidade'] && city.municipio.microrregiao.mesorregiao.UF.sigla == emp['estado'])[0].municipio.id : "",
                estado_id: cidades.filter( city => city.municipio.microrregiao.mesorregiao.UF.sigla == emp['estado']).length > 0 ? cidades.filter( city => city.municipio.microrregiao.mesorregiao.UF.sigla == emp['estado'])[0].municipio.microrregiao.mesorregiao.UF.id : "",
                bairro: emp['bairro'] ? emp['bairro'] : "",
                unidades_por_andar: emp['unidade andar'] ? emp['unidade andar'] : "",
                preco_condominio: emp['valor max'],
                excluido: emp['publicar'] == 'Não' ? false : true,
                caracteristicas: carac,
                fotos: fotos,
                desc: emp['descrição'],
                cep: emp['cep'] ? emp['cep'] : ""
            }
            // if ( empreendFotos.length > 0 ) {
                this.setState({
                    empreendimento: [...this.state.empreendimento,emp['nome']]
                })
                db.firestore().doc(`empresas/GeOvpcrTxJM0XLknXn3j/empreendimentos/${emp['id empreendimento']}`).set({...empreend})
                // return true;
            // }
        }
    }
    MigraImoveis = async () => {
        var db = this.db();
        var bairro_disponiveis = [];
        for await ( let imo of Imoveis.Imoveis) {
            let template = {
                caracteristicas: [],
                banheiros: '',
                preço_condominio: 0,
                fotos: [],
                fotos_thumbnail: [],
                CEP_confirmado: true,
                CEP_error: false,
                excluido: false,
                agenciador_id: "26DQH9COCYNtiiOFGWTVHzmSl9o1",
                codigo: '',
                db_id: '',
                aut_venda: [],
                aut_visita: [],
                cadastrador_id: "",
                campos_personalizados_values: [],
                created_at: new Date(),
                proprietario_id: '',
                bairro: '',
                rua: '',
                número: '',
                estado: {
                id: '',
                nome: '',
                value: ''
                },
                CEP: '',
                cidade: {
                id: '',
                nome: '',
                value: ''
                },
                exibirEndereco: true,
                vagas: '',
                suítes: '',
                dormitórios: '',
                tipo: '',
                aceita_permuta: false,
                mostrar_permuta_no_site: false,
                venda_exibir_valor_no_site: true,
                permuta_desc: "",
                area_total: '',
                area_construída: "",
                area_privativa: "",
                area_útil: "",
                preço_venda: '',
                preço_venda_desconto: 0,
                preço_locação: 0,
                preço_locação_desconto: 0,
                venda_autorizacao: false,
                venda_financiamento: false,
                locação_exibir_valor_no_site: false,
                locação_autorizacao: false,
                bloqueio_fotos: false,
                locação: false,
                venda: true,
                IPTU: 0,
                titulo: "",
                descrição: '',
                desc_interna: "",
                video_youtube: "",
                palavras_chaves: "",
                link_tour_virtual: "",
                inativo: false,
                destaque: ""
            }
            var results = []
            var e = 0;
            let imo_import = await db.firestore().doc(`empresas/GeOvpcrTxJM0XLknXn3j/imoveis/${imo['id imovel']}`).get();
            imo_import = await imo_import.data();
            let id_estado = cidades.filter( city => city.municipio.microrregiao.mesorregiao.UF.sigla == imo['estado']).length > 0 ? cidades.filter( city => city.municipio.microrregiao.mesorregiao.UF.sigla == imo['estado'])[0].municipio.microrregiao.mesorregiao.UF.id : "";
            let cidade = cidades.filter( city => city.nome == imo['cidade'] && city.municipio.microrregiao.mesorregiao.UF.sigla == imo['estado']).length > 0 ? cidades.filter( city => city.nome == imo['cidade'] && city.municipio.microrregiao.mesorregiao.UF.sigla == imo['estado'])[0].municipio.id : "";

            let caracteristica = 
            [   
                {
                    nome: imo['fgts'] == "Sim" ? 'FGTS' : 'FGTS',
                    value: imo['fgts'] == "Sim" ? true : false,
                },
                {
                    nome: imo['agua'] == "Sim" ? 'Água' : 'Água',
                    value: imo['agua'] == "Sim" ? true : false
                },
                {
                    nome: imo['esgoto'] == "Sim" ? 'Esgoto' : 'Esgoto',
                    value: imo['esgoto'] == "Sim" ? true : false,
                },
                {
                    nome: imo['energia'] == "Sim" ? 'Energia' : 'Energia',
                    value: imo['energia'] == "Sim" ? true : false
                },
                {
                    nome: imo['pavimentacao'] == "Sim" ? 'Pavimentação' : 'Pavimentação',
                    value: imo['pavimentacao'] == "Sim" ? false : true
                },
                {
                    nome: imo['cozinha'] == "Sim" ? 'Cozinha' : 'Cozinha',
                    value: imo['cozinha'] == "Sim" ? true : false
                },
                {
                    nome: imo['copa'] == "Sim" ? 'Copa' : 'Copa',
                    value: imo['copa'] == "Sim" ? true : false
                },
                {
                    nome: imo['lavanderia'] == "Sim" ? 'Lavanderia' : 'Lavanderia',
                    value: imo['lavanderia'] == "Sim" ? true : false
                },
                {
                    nome: imo['banheiro empregada'] == "Sim" ? 'Banheiro Empregada' : 'Banheiro Empregada',
                    value: imo['banheiro empregada'] == "Sim" ? true : false
                },
                {
                    nome: imo['despensa'] == "Sim" ? 'Despensa' : 'Despensa',
                    value: imo['despensa'] == "Sim" ? true : false
                },
                {
                    nome: imo['area serviço'] == "Sim" ? 'Area de Serviço' : 'Area de Serviço',
                    value: imo['area serviço'] == "Sim" ? true : false
                },
                {
                    nome: imo['edicula'] == "Sim" ? 'Edicula' : 'Edicula',
                    value: imo['edicula'] == "Sim" ? true : false
                },
                {
                    nome: imo['zelador'] == "Sim" ? 'Zelador' : 'Zelador',
                    value: imo['zelador'] == "Sim" ? true : false
                },
                {
                    nome: imo['churrasqueira'] == "Sim" ? 'Churrasqueira' : 'Churrasqueira',
                    value: imo['churrasqueira'] == "Sim" ? true : false
                },
                {
                    nome: imo['adega'] == "Sim" ? 'Adega' : 'Adega',
                    value: imo['adega'] == "Sim" ? true : false
                },
                {
                    nome: imo['quadra poliesportiva'] == "Sim" ? 'Quadra Poliesportiva' : 'Quadra Poliesportiva',
                    value: imo['quadra poliesportiva'] == "Sim" ? true : false
                },
                {
                    nome: imo['sauna'] == "Sim" ? 'Sauna' : 'Sauna',
                    value: imo['sauna'] == "Sim" ? true : false
                },
                {
                    nome: imo['vestiario'] == "Sim" ? 'Vestiario' : 'Vestiario',
                    value: imo['vestiario'] == "Sim" ? true : false
                },
                {
                    nome: imo['campo futebol'] == "Sim" ? 'Campo Futebol' : 'Campo Futebol',
                    value: imo['campo futebol'] == "Sim" ? true : false
                },
                {
                    nome: imo['lavabo'] == "Sim" ? 'Lavabo' : 'Lavabo',
                    value: imo['lavabo'] == "Sim" ? true : false
                },
                {
                    nome: imo['varanda'] == "Sim" ? 'Varanda' : 'Varanda',
                    value: imo['varanda'] == "Sim" ? true : false
                },
                {
                    nome: imo['sacada'] == "Sim" ? 'Sacada' : 'Sacada',
                    value: imo['sacada'] == "Sim" ? true : false
                },
                {
                    nome: imo['hidro'] == "Sim" ? 'Hidro' : 'Hidro',
                    value: imo['hidro'] == "Sim" ? true : false
                },
                {
                    nome: imo['armario cozinha'] == "Sim" ? 'Armario Cozinha' : 'Armario Cozinha',
                    value: imo['armario cozinha'] == "Sim" ? true : false
                },
                {
                    nome: imo['armario closet'] == "Sim" ? 'Armario Closet' : 'Armario Closet',
                    value: imo['armario closet'] == "Sim" ? true : false
                },
                {
                    nome: imo['armario banheiro'] == "Sim" ? 'Armario banheiro' : 'Armario banheiro',
                    value: imo['armario banheiro'] == "Sim" ? true : false
                },
                {
                    nome: imo['decorado'] == "Sim" ? 'Decorado' : 'Decorado',
                    value: imo['decorado'] == "Sim" ? true : false
                },
                {
                    nome: imo['telefone'] == "Sim" ? 'Telefone' : 'Telefone',
                    value: imo['telefone'] == "Sim" ? true : false
                },
                {
                    nome: imo['tv cabo'] == "Sim" ? 'Tv cabo' : 'Tv cabo',
                    value: imo['tv cabo'] == "Sim" ? true : false
                },
                {
                    nome: imo['quintal'] == "Sim" ? 'Quintal' : 'Quintal',
                    value: imo['quintal'] == "Sim" ? true : false
                },
                {
                    nome: imo['escritorio'] == "Sim" ? 'Escritorio' : 'Escritorio',
                    value: imo['escritorio'] == "Sim" ? true : false
                },
                {
                    nome: imo['alarme'] == "Sim" ? 'Alarme' : 'Alarme',
                    value: imo['alarme'] == "Sim" ? true : false
                },
                {
                    nome: imo['portao'] == "Sim" ? 'Portao' : 'Portao',
                    value: imo['portao'] == "Sim" ? true : false
                },
                {
                    nome: imo['deposito'] == "Sim" ? 'Deposito' : 'Deposito',
                    value: imo['deposito'] == "Sim" ? true : false
                },
                {
                    nome: imo['terraco'] == "Sim" ? 'Terraco' : 'Terraco',
                    value: imo['terraco'] == "Sim" ? true : false
                },
                {
                    nome: imo['jardim inverno'] == "Sim" ? 'Jardim Inverno' : 'Jardim Inverno',
                    value: imo['jardim inverno'] == "Sim" ? true : false
                },
                {
                    nome: imo['pe direito duplo'] == "Sim" ? 'Pe Direito Duplo' : 'Pe Direito Duplo',
                    value: imo['pe direito duplo'] == "Sim" ? true : false
                },
                {
                    nome: imo['lareira'] == "Sim" ? 'Lareira' : 'Lareira',
                    value: imo['lareira'] == "Sim" ? true : false
                },
                {
                    nome: imo['mobiliado'] == "Sim" ? 'Mobiliado' : 'Mobiliado',
                    value: imo['mobiliado'] == "Sim" ? true : false
                },
                {
                    nome: imo['reflorestamento'] == "Sim" ? 'Reflorestamento' : 'Reflorestamento',
                    value: imo['reflorestamento'] == "Sim" ? true : false
                },
                {
                    nome: imo['piscicultura'] == "Sim" ? 'Piscicultura' : 'Piscicultura',
                    value: imo['piscicultura'] == "Sim" ? true : false
                },
                {
                    nome: imo['ovinocultura'] == "Sim" ? 'Ovinocultura' : 'Ovinocultura',
                    value: imo['ovinocultura'] == "Sim" ? true : false
                },
                {
                    nome: imo['caprinocultura'] == "Sim" ? 'Caprinocultura' : 'Caprinocultura',
                    value: imo['caprinocultura'] == "Sim" ? true : false
                },
                {
                    nome: imo['equinocultura'] == "Sim" ? 'Equinocultura' : 'Equinocultura',
                    value: imo['equinocultura'] == "Sim" ? true : false
                },
                {
                    nome: imo['ofuro'] == "Sim" ? 'Ofuro' : 'Ofuro',
                    value: imo['ofuro'] == "Sim" ? true : false
                },
                {
                    nome: imo['estacao gas'] == "Sim" ? 'Estacao gas' : 'Estacao gas',
                    value: imo['estacao gas'] == "Sim" ? true : false
                },
                {
                    nome: imo['doca'] == "Sim" ? 'Doca' : 'Doca',
                    value: imo['doca'] == "Sim" ? true : false
                },
                {
                    nome: imo['cabine primaria'] == "Sim" ? 'Cabine Primaria' : 'Cabine Primaria',
                    value: imo['cabine primaria'] == "Sim" ? true : false
                },
                {
                    nome: imo['kva cabine primaria'] == "Sim" ? 'kva cabine primaria' : 'kva cabine primaria',
                    value: imo['kva cabine primaria'] == "Sim" ? true : false
                },
                {
                    nome: imo['ponte rolante'] == "Sim" ? 'ponte rolante' : 'ponte rolante',
                    value: imo['ponte rolante'] == "Sim" ? true : false
                },
                {
                    nome: imo['Divisoria'] == "Sim" ? 'Divisoria' : 'Divisoria',
                    value: imo['Divisoria'] == "Sim" ? true : false
                },
                {
                    nome: imo['aquecimento solar'] == "Sim" ? 'Aquecimento Solar' : 'Aquecimento Solar',
                    value: imo['aquecimento solar'] == "Sim" ? true : false
                },
                {
                    nome: imo['recepcao'] == "Sim" ? 'Recepcao' : 'Recepcao',
                    value: imo['recepcao'] == "Sim" ? true : false
                },
                {
                    nome: imo['guarita'] == "Sim" ? 'Guarita' : 'Guarita',
                    value: imo['guarita'] == "Sim" ? true : false
                },
                {
                    nome: imo['gerador'] == "Sim" ? 'Gerador' : 'Gerador',
                    value: imo['gerador'] == "Sim" ? true : false
                },
                {
                    nome: imo['mezanino'] == "Sim" ? 'mezanino' : 'mezanino',
                    value: imo['mezanino'] == "Sim" ? true : false
                },
                {
                    nome: imo['litoral'] == "Sim" ? 'litoral' : 'litoral',
                    value: imo['litoral'] == "Sim" ? true : false
                },
                {
                    nome: imo['vista mar'] == "Sim" ? 'vista mar' : 'vista mar',
                    value: imo['vista mar'] == "Sim" ? true : false
                },
                {
                    nome: imo['pe na areia'] == "Sim" ? 'pe na areia' : 'pe na areia',
                    value: imo['pe na areia'] == "Sim" ? true : false
                },
                {
                    nome: imo['armario dormitorio'] == "Sim" ? 'armario dormitorio' : 'armario dormitorio',
                    value: imo['armario dormitorio'] == "Sim" ? true : false
                },
                {
                    nome: imo['ar'] == "Sim" ? 'ar' : 'ar',
                    value: imo['ar'] == "Sim" ? true : false
                },
                {
                    nome: imo['elevador'] == "Sim" ? 'elevador' : 'elevador',
                    value: imo['elevador'] == "Sim" ? true : false
                },
                {
                    nome: imo['altura pe direito'] == "Sim" ? 'altura pe direito' : 'altura pe direito',
                    value: imo['altura pe direito'] == "Sim" ? true : false
                },
                {
                    nome: imo['220v'] == "Sim" ? '220v' : '220v',
                    value: imo['220v'] == "Sim" ? true : false
                },
                {
                    nome: imo['330v'] == "Sim" ? '330v' : '330v',
                    value: imo['330v'] == "Sim" ? true : false
                },
                {
                    nome: imo['bifasico'] == "Sim" ? 'bifasico' : 'bifasico',
                    value: imo['bifasico'] == "Sim" ? true : false
                },
                {
                    nome: imo['trifasico'] == "Sim" ? 'trifasico' : 'trifasico',
                    value: imo['trifasico'] == "Sim" ? true : false
                },
                {
                    nome: imo['area escritorio'] == "Sim" ? 'area escritorio' : 'area escritorio',
                    value: imo['area escritorio'] == "Sim" ? true : false
                },
                {
                    nome: imo['garagem caminhao'] == "Sim" ? 'Garagem caminhao' : 'Garagem caminhao',
                    value: imo['garagem caminhao'] == "Sim" ? true : false
                },
                {
                    nome: imo['garagem caminhao'] == "Sim" ? 'Garagem caminhao' : 'Garagem caminhao',
                    value: imo['garagem caminhao'] == "Sim" ? true : false
                },
                {
                    nome: imo['piscina'] == "Sim" ? 'piscina' : 'piscina',
                    value: imo['piscina'] == "Sim" ? true : false
                },
                {
                    nome: imo['piso frio'] == "Sim" ? 'piso frio' : 'piso frio',
                    value: imo['piso frio'] == "Sim" ? true : false
                },
                {
                    nome: imo['piso elevado'] == "Sim" ? 'piso elevado' : 'piso elevado',
                    value: imo['piso elevado'] == "Sim" ? true : false
                },
                {
                    nome: imo['carpete'] == "Sim" ? 'carpete' : 'carpete',
                    value: imo['carpete'] == "Sim" ? true : false
                },
                {
                    nome: imo['beira mar'] == "Sim" ? 'beira mar' : 'beira mar',
                    value: imo['beira mar'] == "Sim" ? true : false
                },
                {
                    nome: imo['distancia costa'] == "Sim" ? 'distancia costa' : 'distancia costa',
                    value: imo['distancia costa'] == "Sim" ? true : false
                },
                {
                    nome: imo['silvicultura'] == "Sim" ? 'silvicultura' : 'silvicultura',
                    value: imo['silvicultura'] == "Sim" ? true : false
                },
                {
                    nome: imo['graos'] == "Sim" ? 'graos' : 'graos',
                    value: imo['graos'] == "Sim" ? true : false
                },
                {
                    nome: imo['pecuaria'] == "Sim" ? 'pecuaria' : 'pecuaria',
                    value: imo['pecuaria'] == "Sim" ? true : false
                },
                {
                    nome: imo['pastagem'] == "Sim" ? 'pastagem' : 'pastagem',
                    value: imo['pastagem'] == "Sim" ? true : false
                },
                {
                    nome: imo['fruticultura'] == "Sim" ? 'fruticultura' : 'fruticultura',
                    value: imo['fruticultura'] == "Sim" ? true : false
                },
                {
                    nome: imo['barracao'] == "Sim" ? 'barracao' : 'barracao',
                    value: imo['barracao'] == "Sim" ? true : false
                },
                {
                    nome: imo['lavrador'] == "Sim" ? 'lavrador' : 'lavrador',
                    value: imo['lavrador'] == "Sim" ? true : false
                },
                {
                    nome: imo['estabulo'] == "Sim" ? 'estabulo' : 'estabulo',
                    value: imo['estabulo'] == "Sim" ? true : false
                },
                {
                    nome: imo['mangueiro'] == "Sim" ? 'mangueiro' : 'mangueiro',
                    value: imo['mangueiro'] == "Sim" ? true : false
                },
                {
                    nome: imo['garagem maquinario'] == "Sim" ? 'garagem maquinario' : 'garagem maquinario',
                    value: imo['garagem maquinario'] == "Sim" ? true : false
                },
                {
                    nome: imo['maquinario'] == "Sim" ? 'maquinario' : 'maquinario',
                    value: imo['maquinario'] == "Sim" ? true : false
                },
                {
                    nome: imo['tanque peixe'] == "Sim" ? 'tanque peixe' : 'tanque peixe',
                    value: imo['tanque peixe'] == "Sim" ? true : false
                },
                {
                    nome: imo['piso laminado'] == "Sim" ? 'piso laminado' : 'piso laminado',
                    value: imo['piso laminado'] == "Sim" ? true : false
                },
                {
                    nome: imo['piso taco'] == "Sim" ? 'piso taco' : 'piso taco',
                    value: imo['piso taco'] == "Sim" ? true : false
                },
                {
                    nome: imo['carpete madeira'] == "Sim" ? 'carpete madeira' : 'carpete madeira',
                    value: imo['carpete madeira'] == "Sim" ? true : false
                },
                {
                    nome: imo['carpete nylon'] == "Sim" ? 'carpete nylon' : 'carpete nylon',
                    value: imo['carpete nylon'] == "Sim" ? true : false
                },
                {
                    nome: imo['carpete acrilico'] == "Sim" ? 'carpete acrilico' : 'carpete acrilico',
                    value: imo['carpete acrilico'] == "Sim" ? true : false
                },
                {
                    nome: imo['cimento queimado'] == "Sim" ? 'cimento queimado' : 'cimento queimado',
                    value: imo['cimento queimado'] == "Sim" ? true : false
                },
                {
                    nome: imo['piso emborrachado'] == "Sim" ? 'piso emborrachado' : 'piso emborrachado',
                    value: imo['piso emborrachado'] == "Sim" ? true : false
                },
                {
                    nome: imo['piso ardosia'] == "Sim" ? 'piso ardosia' : 'piso ardosia',
                    value: imo['piso ardosia'] == "Sim" ? true : false
                },
                {
                    nome: imo['piso bloquete'] == "Sim" ? 'piso bloquete' : 'piso bloquete',
                    value: imo['piso bloquete'] == "Sim" ? true : false
                },
                {
                    nome: imo['pomar'] == "Sim" ? 'pomar' : 'pomar',
                    value: imo['pomar'] == "Sim" ? true : false
                },
                {
                    nome: imo['turismo rural'] == "Sim" ? 'turismo rural' : 'turismo rural',
                    value: imo['turismo rural'] == "Sim" ? true : false
                },
                {
                    nome: imo['artesiano'] == "Sim" ? 'artesiano' : 'artesiano',
                    value: imo['artesiano'] == "Sim" ? true : false
                },
                {
                    nome: imo['retiros'] == "Sim" ? 'retiros' : 'retiros',
                    value: imo['retiros'] == "Sim" ? true : false
                },
                {
                    nome: imo['lago'] == "Sim" ? 'lago' : 'lago',
                    value: imo['lago'] == "Sim" ? true : false
                },
                {
                    nome: imo['rio'] == "Sim" ? 'rio' : 'rio',
                    value: imo['rio'] == "Sim" ? true : false
                },
                {
                    nome: imo['reserva local'] == "Sim" ? 'reserva local' : 'reserva local',
                    value: imo['reserva local'] == "Sim" ? true : false
                },
                {
                    nome: imo['varanda gourmet'] == "Sim" ? 'varanda gourmet' : 'varanda gourmet',
                    value: imo['varanda gourmet'] == "Sim" ? true : false
                },
                {
                    nome: imo['solarium'] == "Sim" ? 'solarium' : 'solarium',
                    value: imo['solarium'] == "Sim" ? true : false
                },
                {
                    nome: imo['dormitorio reversivel'] == "Sim" ? 'dormitório reversivel' : 'dormitório reversivel',
                    value: imo['dormitorio reversivel'] == "Sim" ? true : false
                },
                {
                    nome: imo['armario sala'] == "Sim" ? 'Armário sala' : 'Armario sala',
                    value: imo['armario sala'] == "Sim" ? true : false
                },
                {
                    nome: imo['armario escritorio'] == "Sim" ? 'Armário escritorio' : 'Armário escritorio',
                    value: imo['armario escritorio'] == "Sim" ? true : false
                },
                {
                    nome: imo['armario home theater'] == "Sim" ? 'Armário home theater' : 'Armário home theater',
                    value: imo['armario home theater'] == "Sim" ? true : false
                },
                {
                    nome: imo['armario dorm. empregada'] == "Sim" ? 'Armário dorm. empregada' : 'Armário dorm. empregada',
                    value: imo['armario dorm. empregada'] == "Sim" ? true : false
                },
                {
                    nome: imo['armario area de servico'] == "Sim" ? 'Armário area de servico' : 'Armário area de servico',
                    value: imo['armario area de servico'] == "Sim" ? true : false
                },
                {
                    nome: imo['piso aquecido'] == "Sim" ? 'piso aquecido' : 'piso aquecido',
                    value: imo['piso aquecido'] == "Sim" ? true : false
                },
                {
                    nome: imo['contra piso'] == "Sim" ? 'contra piso' : 'contra piso',
                    value: imo['contra piso'] == "Sim" ? true : false
                },
                {
                    nome: imo['piso granito'] == "Sim" ? 'piso granito' : 'piso granito',
                    value: imo['piso granito'] == "Sim" ? true : false
                },
                {
                    nome: imo['piso marmore'] == "Sim" ? 'piso marmore' : 'piso marmore',
                    value: imo['piso marmore'] == "Sim" ? true : false
                },
                {
                    nome: imo['piso porcelanato'] == "Sim" ? 'piso porcelanato' : 'piso porcelanato',
                    value: imo['piso porcelanato'] == "Sim" ? true : false
                },
                {
                    nome: imo['sem comdomínio'] == "Sim" ? 'sem comdomínio' : 'sem comdomínio',
                    value: imo['sem comdomínio'] == "Sim" ? true : false
                },
                {
                    nome: imo['ano construcao'] == "Sim" ? 'ano construcao' : 'ano construcao',
                    value: imo['ano construcao'] == "Sim" ? true : false
                }
            ]
            let caracteristica_disponiveis = 
            [   
                {
                    nome: imo['fgts'] == "Sim" ? 'FGTS' : 'FGTS',
                    id: imo['fgts'] == "Sim" ? 'FGTS' : 'FGTS',
                },
                {
                    nome: imo['agua'] == "Sim" ? 'Água' : 'Água',
                    id: imo['agua'] == "Sim" ? 'Água' : 'Água'
                },
                {
                    nome: imo['esgoto'] == "Sim" ? 'Esgoto' : 'Esgoto',
                    id: imo['esgoto'] == "Sim" ? 'Esgoto' : 'Esgoto',
                },
                {
                    nome: imo['energia'] == "Sim" ? 'Energia' : 'Energia',
                    id: imo['energia'] == "Sim" ? 'Energia' : 'Energia'
                },
                {
                    nome: imo['pavimentacao'] == "Sim" ? 'Pavimentação' : 'Pavimentação',
                    id: imo['pavimentacao'] == "Sim" ? 'Pavimentação' : 'Pavimentação',
                },
                {
                    nome: imo['cozinha'] == "Sim" ? 'Cozinha' : 'Cozinha',
                    id: imo['cozinha'] == "Sim" ? 'Cozinha' : 'Cozinha'
                },
                {
                    nome: imo['copa'] == "Sim" ? 'Copa' : 'Copa',
                    id: imo['copa'] == "Sim" ? 'Copa' : 'Copa',
                },
                {
                    nome: imo['lavanderia'] == "Sim" ? 'Lavanderia' : 'Lavanderia',
                    id: imo['lavanderia'] == "Sim" ? 'Lavanderia' : 'Lavanderia',
                },
                {
                    nome: imo['banheiro empregada'] == "Sim" ? 'Banheiro Empregada' : 'Banheiro Empregada',
                    id: imo['banheiro empregada'] == "Sim" ? 'Banheiro Empregada' : 'Banheiro Empregada'
                },
                {
                    nome: imo['despensa'] == "Sim" ? 'Despensa' : 'Despensa',
                    id: imo['despensa'] == "Sim" ? 'Despensa' : 'Despensa'
                },
                {
                    nome: imo['area serviço'] == "Sim" ? 'Area de Serviço' : 'Area de Serviço',
                    id: imo['area serviço'] == "Sim" ? 'Area de Serviço' : 'Area de Serviço',
                },
                {
                    nome: imo['edicula'] == "Sim" ? 'Edicula' : 'Edicula',
                    id: imo['edicula'] == "Sim" ? 'Edicula' : 'Edicula'
                },
                {
                    nome: imo['zelador'] == "Sim" ? 'Zelador' : 'Zelador',
                    id: imo['zelador'] == "Sim" ? 'Zelador' : 'Zelador'
                },
                {
                    nome: imo['churrasqueira'] == "Sim" ? 'Churrasqueira' : 'Churrasqueira',
                    id: imo['churrasqueira'] == "Sim" ? 'Churrasqueira' : 'Churrasqueira'
                },
                {
                    nome: imo['adega'] == "Sim" ? 'Adega' : 'Adega',
                    id: imo['adega'] == "Sim" ? 'Adega' : 'Adega'
                },
                {
                    nome: imo['quadra poliesportiva'] == "Sim" ? 'Quadra Poliesportiva' : 'Quadra Poliesportiva',
                    id: imo['quadra poliesportiva'] == "Sim" ? 'Quadra Poliesportiva' : 'Quadra Poliesportiva',
                },
                {
                    nome: imo['sauna'] == "Sim" ? 'Sauna' : 'Sauna',
                    id: imo['sauna'] == "Sim" ? 'Sauna' : 'Sauna'
                },
                {
                    nome: imo['vestiario'] == "Sim" ? 'Vestiario' : 'Vestiario',
                    id: imo['vestiario'] == "Sim" ? 'Vestiario' : 'Vestiario'
                },
                {
                    nome: imo['campo futebol'] == "Sim" ? 'Campo Futebol' : 'Campo Futebol',
                    id: imo['campo futebol'] == "Sim" ? 'Campo Futebol' : 'Campo Futebol'
                },
                {
                    nome: imo['lavabo'] == "Sim" ? 'Lavabo' : 'Lavabo',
                    id: imo['lavabo'] == "Sim" ? 'Lavabo' : 'Lavabo'
                },
                {
                    nome: imo['varanda'] == "Sim" ? 'Varanda' : 'Varanda',
                    id: imo['varanda'] == "Sim" ? 'Varanda' : 'Varanda',
                },
                {
                    nome: imo['sacada'] == "Sim" ? 'Sacada' : 'Sacada',
                    id: imo['sacada'] == "Sim" ? 'Sacada' : 'Sacada',
                },
                {
                    nome: imo['hidro'] == "Sim" ? 'Hidro' : 'Hidro',
                    id: imo['hidro'] == "Sim" ? 'Hidro' : 'Hidro',
                },
                {
                    nome: imo['armario cozinha'] == "Sim" ? 'Armario Cozinha' : 'Armario Cozinha',
                    id: imo['armario cozinha'] == "Sim" ? 'Armario Cozinha' : 'Armario Cozinha',
                },
                {
                    nome: imo['armario closet'] == "Sim" ? 'Armario Closet' : 'Armario Closet',
                    id: imo['armario closet'] == "Sim" ? 'Armario Closet' : 'Armario Closet'
                },
                {
                    nome: imo['armario banheiro'] == "Sim" ? 'Armario banheiro' : 'Armario banheiro',
                    id: imo['armario banheiro'] == "Sim" ? 'Armario banheiro' : 'Armario banheiro'
                },
                {
                    nome: imo['decorado'] == "Sim" ? 'Decorado' : 'Decorado',
                    id: imo['decorado'] == "Sim" ? 'Decorado' : 'Decorado',
                },
                {
                    nome: imo['telefone'] == "Sim" ? 'Telefone' : 'Telefone',
                    id: imo['telefone'] == "Sim" ? 'Telefone' : 'Telefone'
                },
                {
                    nome: imo['tv cabo'] == "Sim" ? 'Tv cabo' : 'Tv cabo',
                    id: imo['tv cabo'] == "Sim" ? 'Tv cabo' : 'Tv cabo',
                },
                {
                    nome: imo['quintal'] == "Sim" ? 'Quintal' : 'Quintal',
                    id: imo['quintal'] == "Sim" ? 'Quintal' : 'Quintal'
                },
                {
                    nome: imo['escritorio'] == "Sim" ? 'Escritorio' : 'Escritorio',
                    id: imo['escritorio'] == "Sim" ? 'Escritorio' : 'Escritorio'
                },
                {
                    nome: imo['alarme'] == "Sim" ? 'Alarme' : 'Alarme',
                    id: imo['alarme'] == "Sim" ? 'Alarme' : 'Alarme',
                },
                {
                    nome: imo['portao'] == "Sim" ? 'Portao' : 'Portao',
                    id: imo['portao'] == "Sim" ? 'Portao' : 'Portao',
                },
                {
                    nome: imo['deposito'] == "Sim" ? 'Deposito' : 'Deposito',
                    id: imo['deposito'] == "Sim" ? 'Deposito' : 'Deposito',
                },
                {
                    nome: imo['terraco'] == "Sim" ? 'Terraco' : 'Terraco',
                    id: imo['terraco'] == "Sim" ? 'Terraco' : 'Terraco',
                },
                {
                    nome: imo['jardim inverno'] == "Sim" ? 'Jardim Inverno' : 'Jardim Inverno',
                    id: imo['jardim inverno'] == "Sim" ? 'Jardim Inverno' : 'Jardim Inverno'
                },
                {
                    nome: imo['pe direito duplo'] == "Sim" ? 'Pe Direito Duplo' : 'Pe Direito Duplo',
                    id: imo['pe direito duplo'] == "Sim" ? 'Pe Direito Duplo' : 'Pe Direito Duplo'
                },
                {
                    nome: imo['lareira'] == "Sim" ? 'Lareira' : 'Lareira',
                    id: imo['lareira'] == "Sim" ? 'Lareira' : 'Lareira',
                },
                {
                    nome: imo['mobiliado'] == "Sim" ? 'Mobiliado' : 'Mobiliado',
                    id: imo['mobiliado'] == "Sim" ? 'Mobiliado' : 'Mobiliado',
                },
                {
                    nome: imo['reflorestamento'] == "Sim" ? 'Reflorestamento' : 'Reflorestamento',
                    id: imo['reflorestamento'] == "Sim" ? 'Reflorestamento' : 'Reflorestamento'
                },
                {
                    nome: imo['piscicultura'] == "Sim" ? 'Piscicultura' : 'Piscicultura',
                    id: imo['piscicultura'] == "Sim" ? 'Piscicultura' : 'Piscicultura',
                },
                {
                    nome: imo['ovinocultura'] == "Sim" ? 'Ovinocultura' : 'Ovinocultura',
                    id: imo['ovinocultura'] == "Sim" ? 'Ovinocultura' : 'Ovinocultura',
                },
                {
                    nome: imo['caprinocultura'] == "Sim" ? 'Caprinocultura' : 'Caprinocultura',
                    id: imo['caprinocultura'] == "Sim" ? 'Caprinocultura' : 'Caprinocultura',
                },
                {
                    nome: imo['equinocultura'] == "Sim" ? 'Equinocultura' : 'Equinocultura',
                    id: imo['equinocultura'] == "Sim" ? 'Equinocultura' : 'Equinocultura',
                },
                {
                    nome: imo['ofuro'] == "Sim" ? 'Ofuro' : 'Ofuro',
                    id: imo['ofuro'] == "Sim" ? 'Ofuro' : 'Ofuro',
                },
                {
                    nome: imo['estacao gas'] == "Sim" ? 'Estacao gas' : 'Estacao gas',
                    id: imo['estacao gas'] == "Sim" ? 'Estacao gas' : 'Estacao gas',
                },
                {
                    nome: imo['doca'] == "Sim" ? 'Doca' : 'Doca',
                    id: imo['doca'] == "Sim" ? 'Doca' : 'Doca',
                },
                {
                    nome: imo['cabine primaria'] == "Sim" ? 'Cabine Primaria' : 'Cabine Primaria',
                    id: imo['cabine primaria'] == "Sim" ? 'Cabine Primaria' : 'Cabine Primaria',
                },
                {
                    nome: imo['kva cabine primaria'] == "Sim" ? 'kva cabine primaria' : 'kva cabine primaria',
                    id: imo['kva cabine primaria'] == "Sim" ? 'kva cabine primaria' : 'kva cabine primaria',
                },
                {
                    nome: imo['ponte rolante'] == "Sim" ? 'ponte rolante' : 'ponte rolante',
                    id: imo['ponte rolante'] == "Sim" ? 'ponte rolante' : 'ponte rolante',
                },
                {
                    nome: imo['Divisoria'] == "Sim" ? 'Divisoria' : 'Divisoria',
                    id: imo['Divisoria'] == "Sim" ? 'Divisoria' : 'Divisoria',
                },
                {
                    nome: imo['aquecimento solar'] == "Sim" ? 'Aquecimento Solar' : 'Aquecimento Solar',
                    id: imo['aquecimento solar'] == "Sim" ? 'Aquecimento Solar' : 'Aquecimento Solar',
                },
                {
                    nome: imo['recepcao'] == "Sim" ? 'Recepcao' : 'Recepcao',
                    id: imo['recepcao'] == "Sim" ? 'Recepcao' : 'Recepcao',
                },
                {
                    nome: imo['guarita'] == "Sim" ? 'Guarita' : 'Guarita',
                    id: imo['guarita'] == "Sim" ? 'Guarita' : 'Guarita'
                },
                {
                    nome: imo['gerador'] == "Sim" ? 'Gerador' : 'Gerador',
                    id: imo['gerador'] == "Sim" ? 'Gerador' : 'Gerador'
                },
                {
                    nome: imo['mezanino'] == "Sim" ? 'mezanino' : 'mezanino',
                    id: imo['mezanino'] == "Sim" ? 'mezanino' : 'mezanino'
                },
                {
                    nome: imo['litoral'] == "Sim" ? 'litoral' : 'litoral',
                    id: imo['litoral'] == "Sim" ? 'litoral' : 'litoral',
                },
                {
                    nome: imo['vista mar'] == "Sim" ? 'vista mar' : 'vista mar',
                    id: imo['vista mar'] == "Sim" ? 'vista mar' : 'vista mar',
                },
                {
                    nome: imo['pe na areia'] == "Sim" ? 'pe na areia' : 'pe na areia',
                    id: imo['pe na areia'] == "Sim" ? 'pe na areia' : 'pe na areia',
                },
                {
                    nome: imo['armario dormitorio'] == "Sim" ? 'armario dormitorio' : 'armario dormitorio',
                    id: imo['armario dormitorio'] == "Sim" ? 'armario dormitorio' : 'armario dormitorio',
                },
                {
                    nome: imo['ar'] == "Sim" ? 'ar' : 'ar',
                    id: imo['ar'] == "Sim" ? 'ar' : 'ar'
                },
                {
                    nome: imo['elevador'] == "Sim" ? 'elevador' : 'elevador',
                    id: imo['elevador'] == "Sim" ? 'elevador' : 'elevador'
                },
                {
                    nome: imo['altura pe direito'] == "Sim" ? 'altura pe direito' : 'altura pe direito',
                    id: imo['altura pe direito'] == "Sim" ? 'altura pe direito' : 'altura pe direito',
                },
                {
                    nome: imo['220v'] == "Sim" ? '220v' : '220v',
                    id: imo['220v'] == "Sim" ? '220v' : '220v'
                },
                {
                    nome: imo['330v'] == "Sim" ? '330v' : '330v',
                    id: imo['330v'] == "Sim" ? '330v' : '330v',
                },
                {
                    nome: imo['bifasico'] == "Sim" ? 'bifasico' : 'bifasico',
                    id: imo['bifasico'] == "Sim" ? 'bifasico' : 'bifasico',
                },
                {
                    nome: imo['trifasico'] == "Sim" ? 'trifasico' : 'trifasico',
                    id: imo['trifasico'] == "Sim" ? 'trifasico' : 'trifasico'
                },
                {
                    nome: imo['area escritorio'] == "Sim" ? 'area escritorio' : 'area escritorio',
                    id: imo['area escritorio'] == "Sim" ? 'area escritorio' : 'area escritorio'
                },
                {
                    nome: imo['garagem caminhao'] == "Sim" ? 'Garagem caminhao' : 'Garagem caminhao',
                    id: imo['garagem caminhao'] == "Sim" ? 'Garagem caminhao' : 'Garagem caminhao'
                },
                {
                    nome: imo['garagem caminhao'] == "Sim" ? 'Garagem caminhao' : 'Garagem caminhao',
                    id: imo['garagem caminhao'] == "Sim" ? 'Garagem caminhao' : 'Garagem caminhao',
                },
                {
                    nome: imo['piscina'] == "Sim" ? 'piscina' : 'piscina',
                    id: imo['piscina'] == "Sim" ? 'piscina' : 'piscina',
                },
                {
                    nome: imo['piso frio'] == "Sim" ? 'piso frio' : 'piso frio',
                    id: imo['piso frio'] == "Sim" ? 'piso frio' : 'piso frio',
                },
                {
                    nome: imo['piso elevado'] == "Sim" ? 'piso elevado' : 'piso elevado',
                    id: imo['piso elevado'] == "Sim" ? 'piso elevado' : 'piso elevado',
                },
                {
                    nome: imo['carpete'] == "Sim" ? 'carpete' : 'carpete',
                    id: imo['carpete'] == "Sim" ? 'carpete' : 'carpete',
                },
                {
                    nome: imo['beira mar'] == "Sim" ? 'beira mar' : 'beira mar',
                    id: imo['beira mar'] == "Sim" ? 'beira mar' : 'beira mar',
                },
                {
                    nome: imo['distancia costa'] == "Sim" ? 'distancia costa' : 'distancia costa',
                    id: imo['distancia costa'] == "Sim" ? 'distancia costa' : 'distancia costa',
                },
                {
                    nome: imo['silvicultura'] == "Sim" ? 'silvicultura' : 'silvicultura',
                    id: imo['silvicultura'] == "Sim" ? 'silvicultura' : 'silvicultura',
                },
                {
                    nome: imo['graos'] == "Sim" ? 'graos' : 'graos',
                    id: imo['graos'] == "Sim" ? 'graos' : 'graos',
                },
                {
                    nome: imo['pecuaria'] == "Sim" ? 'pecuaria' : 'pecuaria',
                    id: imo['pecuaria'] == "Sim" ? 'pecuaria' : 'pecuaria',
                },
                {
                    nome: imo['pastagem'] == "Sim" ? 'pastagem' : 'pastagem',
                    id: imo['pastagem'] == "Sim" ? 'pastagem' : 'pastagem',
                },
                {
                    nome: imo['fruticultura'] == "Sim" ? 'fruticultura' : 'fruticultura',
                    id: imo['fruticultura'] == "Sim" ? 'fruticultura' : 'fruticultura'
                },
                {
                    nome: imo['barracao'] == "Sim" ? 'barracao' : 'barracao',
                    id: imo['barracao'] == "Sim" ? 'barracao' : 'barracao'
                },
                {
                    nome: imo['lavrador'] == "Sim" ? 'lavrador' : 'lavrador',
                    id: imo['lavrador'] == "Sim" ? 'lavrador' : 'lavrador',
                },
                {
                    nome: imo['estabulo'] == "Sim" ? 'estabulo' : 'estabulo',
                    id: imo['estabulo'] == "Sim" ? 'estabulo' : 'estabulo',
                },
                {
                    nome: imo['mangueiro'] == "Sim" ? 'mangueiro' : 'mangueiro',
                    id: imo['mangueiro'] == "Sim" ? 'mangueiro' : 'mangueiro',
                },
                {
                    nome: imo['garagem maquinario'] == "Sim" ? 'garagem maquinario' : 'garagem maquinario',
                    id: imo['garagem maquinario'] == "Sim" ? 'garagem maquinario' : 'garagem maquinario',
                },
                {
                    nome: imo['maquinario'] == "Sim" ? 'maquinario' : 'maquinario',
                    id: imo['maquinario'] == "Sim" ? 'maquinario' : 'maquinario',
                },
                {
                    nome: imo['tanque peixe'] == "Sim" ? 'tanque peixe' : 'tanque peixe',
                    id: imo['tanque peixe'] == "Sim" ? 'tanque peixe' : 'tanque peixe',
                },
                {
                    nome: imo['piso laminado'] == "Sim" ? 'piso laminado' : 'piso laminado',
                    id: imo['piso laminado'] == "Sim" ? 'piso laminado' : 'piso laminado',
                },
                {
                    nome: imo['piso taco'] == "Sim" ? 'piso taco' : 'piso taco',
                    id: imo['piso taco'] == "Sim" ? 'piso taco' : 'piso taco',
                },
                {
                    nome: imo['carpete madeira'] == "Sim" ? 'carpete madeira' : 'carpete madeira',
                    id: imo['carpete madeira'] == "Sim" ? 'carpete madeira' : 'carpete madeira',
                },
                {
                    nome: imo['carpete nylon'] == "Sim" ? 'carpete nylon' : 'carpete nylon',
                    id: imo['carpete nylon'] == "Sim" ? 'carpete nylon' : 'carpete nylon',
                },
                {
                    nome: imo['carpete acrilico'] == "Sim" ? 'carpete acrilico' : 'carpete acrilico',
                    id: imo['carpete acrilico'] == "Sim" ? 'carpete acrilico' : 'carpete acrilico',
                },
                {
                    nome: imo['cimento queimado'] == "Sim" ? 'cimento queimado' : 'cimento queimado',
                    id: imo['cimento queimado'] == "Sim" ? 'cimento queimado' : 'cimento queimado',
                },
                {
                    nome: imo['piso emborrachado'] == "Sim" ? 'piso emborrachado' : 'piso emborrachado',
                    id: imo['piso emborrachado'] == "Sim" ? 'piso emborrachado' : 'piso emborrachado',
                },
                {
                    nome: imo['piso ardosia'] == "Sim" ? 'piso ardosia' : 'piso ardosia',
                    id: imo['piso ardosia'] == "Sim" ? 'piso ardosia' : 'piso ardosia',
                },
                {
                    nome: imo['piso bloquete'] == "Sim" ? 'piso bloquete' : 'piso bloquete',
                    id: imo['piso bloquete'] == "Sim" ? 'piso bloquete' : 'piso bloquete',
                },
                {
                    nome: imo['pomar'] == "Sim" ? 'pomar' : 'pomar',
                    id: imo['pomar'] == "Sim" ? 'pomar' : 'pomar',
                },
                {
                    nome: imo['turismo rural'] == "Sim" ? 'turismo rural' : 'turismo rural',
                    id: imo['turismo rural'] == "Sim" ? 'turismo rural' : 'turismo rural',
                },
                {
                    nome: imo['artesiano'] == "Sim" ? 'artesiano' : 'artesiano',
                    id: imo['artesiano'] == "Sim" ? 'artesiano' : 'artesiano',
                },
                {
                    nome: imo['retiros'] == "Sim" ? 'retiros' : 'retiros',
                    id: imo['retiros'] == "Sim" ? 'retiros' : 'retiros',
                },
                {
                    nome: imo['lago'] == "Sim" ? 'lago' : 'lago',
                    id: imo['lago'] == "Sim" ? 'lago' : 'lago',
                },
                {
                    nome: imo['rio'] == "Sim" ? 'rio' : 'rio',
                    id: imo['rio'] == "Sim" ? 'rio' : 'rio',
                },
                {
                    nome: imo['reserva local'] == "Sim" ? 'reserva local' : 'reserva local',
                    id: imo['reserva local'] == "Sim" ? 'reserva local' : 'reserva local',
                },
                {
                    nome: imo['varanda gourmet'] == "Sim" ? 'varanda gourmet' : 'varanda gourmet',
                    id: imo['varanda gourmet'] == "Sim" ? 'varanda gourmet' : 'varanda gourmet',
                },
                {
                    nome: imo['solarium'] == "Sim" ? 'solarium' : 'solarium',
                    id: imo['solarium'] == "Sim" ? 'solarium' : 'solarium',
                },
                {
                    nome: imo['dormitorio reversivel'] == "Sim" ? 'dormitório reversivel' : 'dormitório reversivel',
                    id: imo['dormitorio reversivel'] == "Sim" ? 'dormitório reversivel' : 'dormitório reversivel',
                },
                {
                    nome: imo['armario sala'] == "Sim" ? 'Armário sala' : 'Armario sala',
                    id: imo['armario sala'] == "Sim" ? 'Armário sala' : 'Armario sala',
                },
                {
                    nome: imo['armario escritorio'] == "Sim" ? 'Armário escritorio' : 'Armário escritorio',
                    id: imo['armario escritorio'] == "Sim" ? 'Armário escritorio' : 'Armário escritorio',
                },
                {
                    nome: imo['armario home theater'] == "Sim" ? 'Armário home theater' : 'Armário home theater',
                    id: imo['armario home theater'] == "Sim" ? 'Armário home theater' : 'Armário home theater',
                },
                {
                    nome: imo['armario dorm. empregada'] == "Sim" ? 'Armário dorm. empregada' : 'Armário dorm. empregada',
                    id: imo['armario dorm. empregada'] == "Sim" ? 'Armário dorm. empregada' : 'Armário dorm. empregada',
                },
                {
                    nome: imo['armario area de servico'] == "Sim" ? 'Armário area de servico' : 'Armário area de servico',
                    id: imo['armario area de servico'] == "Sim" ? 'Armário area de servico' : 'Armário area de servico',
                },
                {
                    nome: imo['piso aquecido'] == "Sim" ? 'piso aquecido' : 'piso aquecido',
                    id: imo['piso aquecido'] == "Sim" ? 'piso aquecido' : 'piso aquecido'
                },
                {
                    nome: imo['contra piso'] == "Sim" ? 'contra piso' : 'contra piso',
                    id: imo['contra piso'] == "Sim" ? 'contra piso' : 'contra piso'
                },
                {
                    nome: imo['piso granito'] == "Sim" ? 'piso granito' : 'piso granito',
                    id: imo['piso granito'] == "Sim" ? 'piso granito' : 'piso granito',
                },
                {
                    nome: imo['piso marmore'] == "Sim" ? 'piso marmore' : 'piso marmore',
                    id: imo['piso marmore'] == "Sim" ? 'piso marmore' : 'piso marmore'
                },
                {
                    nome: imo['piso porcelanato'] == "Sim" ? 'piso porcelanato' : 'piso porcelanato',
                    id: imo['piso porcelanato'] == "Sim" ? 'piso porcelanato' : 'piso porcelanato',
                },
                {
                    nome: imo['sem comdomínio'] == "Sim" ? 'sem comdomínio' : 'sem comdomínio',
                    id: imo['sem comdomínio'] == "Sim" ? 'sem comdomínio' : 'sem comdomínio',
                },
                {
                    nome: imo['ano construcao'] == "Sim" ? 'ano construcao' : 'ano construcao',
                    id: imo['ano construcao'] == "Sim" ? 'ano construcao' : 'ano construcao'
                }
            ]
            var results = [];
            var e = 0;
            let fotos = ImoveisFotos.ImoveisFotos.filter( imoF => imoF['id imovel'] == imo['id imovel']);
            if ( !imo_import ) {
                for await (let imagem of fotos) {
                    var img = [];
                    
                    let im = await fetch(`http://localhost/test_sockets.php?imagem=${imagem['url grande']}`).then( dados => dados.json()).catch(function(error){ return error});
                    
                    var storageRef = db.storage().ref();
                             
                    var fotoRef = storageRef.child(`empresas/GeOvpcrTxJM0XLknXn3j/${imo['id imovel']}/${imo['id imovel']}_${e}.jpeg`);
                    if (im.caminho) {
                      let snapshot = await fotoRef.putString(im.caminho, 'data_url',{contentType:'image/jpeg'})
                      results.push({
                        destaque: e === 0 ? true : false,
                        height: null,
                        id: `${imo['id imovel']}_${e}`,
                        uploaded: true,
                        width: null,
                        resized: await fotoRef.getDownloadURL(),
                        source:{uri: await fotoRef.getDownloadURL()}
                      })
                      e++;
                      console.log("foto inserida: ",e);
                    }         
                }
            }
            let campos_personalizados = [];
            let c_personalizados = [];
            if( imo['condição iptu'] ) {
                campos_personalizados.push({
                        nome: 'Condição Iptu',
                        tipo: 'text',
                        value: imo['condição iptu']
                    });
                c_personalizados.push({
                    nome: 'Condição Iptu',
                    tipo: 'text',
                    created_at: new Date()
                })
            }
            if ( imo['ocupacao'] ) {
                campos_personalizados.push({
                    nome: 'Ocupação',
                    tipo: 'text',
                    value: imo['ocupacao']
                })
                c_personalizados.push({
                    nome: 'Ocupação',
                    tipo: 'text',
                    created_at: new Date()
                })
            }
            if ( imo['padrao'] ) {
                campos_personalizados.push({
                    nome: 'Padrão',
                    tipo: 'text',
                    value: imo['padrao']
                });
                c_personalizados.push({
                    nome: 'Padrão',
                    tipo: 'text',
                    created_at: new Date()
                })
            }
            if ( imo['isolamento'] ) {
                campos_personalizados.push({
                    nome: 'Isolamento',
                    tipo: 'text',
                    value: imo['isolamento']
                });
                c_personalizados.push({
                    nome: 'Isolamento',
                    tipo: 'text',
                    created_at: new Date()
                })
            }
            if ( imo['tipo isolamento'] ) {
                campos_personalizados.push({
                    nome: 'Tipo de Isolamento',
                    tipo: 'text',
                    value: imo['tipo isolamento']
                });
                c_personalizados.push({
                    nome: 'Tipo de Isolamento',
                    tipo: 'text',
                    created_at: new Date()
                })
            }
            if ( imo['ocupador'] ) {
                campos_personalizados.push({
                    nome: 'Ocupador',
                    tipo: 'text',
                    value: imo['ocupador']
                });
                c_personalizados.push({
                    nome: 'Ocupador',
                    tipo: 'text',
                    created_at: new Date()
                })
            }
            if ( imo['sinal'] ) {
                campos_personalizados.push({
                    nome: 'Sinal',
                    tipo: 'text',
                    value: imo['sinal']
                });
                c_personalizados.push({
                    nome: 'Sinal',
                    tipo: 'text',
                    created_at: new Date()
                })
            }
            if (imo['saldo devedor']) {
                campos_personalizados.push({
                    nome: 'Saldo devedor',
                    tipo: 'text',
                    value: imo['saldo devedor']
                })
            }
            if (imo['observacao sinal']) {
                campos_personalizados.push({
                    nome: 'Observação sinal',
                    tipo: 'text',
                    value: imo['observacao sinal']
                })
            }
            if ( imo['valor tabela'] ) {
                campos_personalizados.push({
                    nome: 'Valor da Tabela',
                    tipo: 'text',
                    value: imo['valor tabela']
                });
                c_personalizados.push({
                    nome: 'Valor da Tabela',
                    tipo: 'text',
                    created_at: new Date()
                })
            }
            if ( imo['desconto'] ) { 
                campos_personalizados.push({
                    nome: 'Desconto',
                    tipo: 'text',
                    value: imo['desconto']
                });
                c_personalizados.push({
                    nome: 'Valor da Tabela',
                    tipo: 'text',
                    created_at: new Date()
                })
            }
            if (imo['tipo vaga']) {
                campos_personalizados.push({
                    nome: 'Tipo da vaga',
                    tipo: 'text',
                    value: imo['tipo vaga']
                });
                c_personalizados.push({
                    nome: 'Tipo da vaga',
                    tipo: 'text',
                    created_at: new Date()
                });
            }
            if ( imo['caracteristica vaga'] ) {
                campos_personalizados.push({
                    nome: 'Caracteristica da vaga',
                    tipo: 'text',
                    value: imo['caracteristica vaga']
                });
                c_personalizados.push({
                    nome: 'Caracteristica da vaga',
                    tipo: 'text',
                    created_at: new Date()
                });
            }
            console.log(results)
            template = {
                ...template,
                db_id: imo['id imovel'],
                proprietario_id: imo['id cliente'] ? imo['id cliente'] : '',
                empreendimento_id: imo['id empreendimento'] ? imo['id empreendimento'] : '',
                banheiros: imo['banheiros'] ? imo['banheiros'] : '',
                dormitórios: imo['dormitorios'] ? imo['dormitorios'] : '',
                vagas: imo['garagens cobertas'] ? imo['garagens cobertas'] : '',
                suítes: imo['suites'] ? imo['suites'] : '',
                created_at: new Date(imo['data cadastro']),
                edited_at: new Date(imo['data atualizacao']),
                codigo: imo['referencia'],
                aceita_permuta: imo['aceita permuta'] == 'Sim' ? true : false,
                tipo: imo['tipo'],
                preço_venda: imo['valor venda'] > 0 ? imo['valor venda'] : '',
                preço_locação: imo['valor locacao'] > 0 ? imo['valor locacao'] : '',
                rua: imo['logradouro'] ? imo['logradouro'] : '',
                CEP: imo['cep'] ? imo['cep'] : '',
                cep: imo['cep'] ? imo['cep'] : '',
                bairro: imo['bairro'] ? imo['bairro'] : '',
                número: imo['numero'] ? imo['numero'] : '',
                complemento: imo['complemento'] ? imo['complemento'] : '',
                lat: imo['dl_latitude'] ? imo['dl_latitude'] : '',
                long: imo['dl_longitude'] ? imo['dl_longitude'] : '',
                preço_condominio: imo['valor condominio'] ? imo['valor condominio'] : '',
                excluido: imo['status'] == 'Vendido' ? true : false,
                area_total: imo['area total'] ? imo['area total'] : '',
                descrição: imo['descricao site'] ? imo['descricao site'] : '',
                area_útil: imo['area util construida'] ? imo['area util construida'] : '',
                area_privativa: imo['area comum'] ? imo['area comum'] : '',
                video_youtube: imo['youtube'],
                titulo: imo['titulo'] ? imo['titulo'] : '',
                desc_interna: imo['descricao geral'] ? imo['descricao geral'] : '',
                IPTU: imo['valor iptu'] ? imo['valor iptu'] : '',
                campos_personalizados_values: campos_personalizados,
                caracteristicas: caracteristica,
                fotos: [...results],
                estado: {
                    id: id_estado,
                    nome: cidades.filter( city => city.municipio.microrregiao.mesorregiao.UF.sigla == imo['estado']).length > 0 ? cidades.filter( city => city.municipio.microrregiao.mesorregiao.UF.sigla == imo['estado'])[0].municipio.microrregiao.mesorregiao.UF.nome : "",
                    value: id_estado
                },
                cidade: {
                    id: cidade,
                    nome: cidades.filter( city => city.nome == imo['cidade'] && city.municipio.microrregiao.mesorregiao.UF.sigla == imo['estado']).length > 0 ? cidades.filter( city => city.nome == imo['cidade'] && city.municipio.microrregiao.mesorregiao.UF.sigla == imo['estado'])[0].municipio.nome : "",
                    value: cidade
                }
            }
            if ( bairro_disponiveis.filter( bairro => bairro.bairro == imo['bairro']).length == 0 && imo['bairro']) {
                bairro_disponiveis.push({
                  bairro: imo['bairro'],
                  cidade: cidade,
                  estado: id_estado,
                })
            }
            if(!imo_import)
                var imovel = db.firestore().doc(`empresas/GeOvpcrTxJM0XLknXn3j/imoveis/${imo['id imovel']}`).set({...template});
            db.firestore().doc(`empresas/GeOvpcrTxJM0XLknXn3j`).update({bairros_disponiveis: bairro_disponiveis,caracteristicas_disponiveis: caracteristica_disponiveis,campos_personalizados: c_personalizados})
            console.log("inserido: ",imo['id imovel'])
            // return true;
        }
    }
    render() {
        return(
            <div className="container">
                {
                    this.state.empreendimento.map( emp => {
                        return(
                            <div>{emp}</div>
                        )
                    })
                }
                {
                    this.state.fotos.map( fotos => {
                        return(
                            <div>{fotos}</div>
                        )
                    })
                }
            </div>
        )
    }
}