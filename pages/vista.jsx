import React, { Component } from 'react';
import firebase from "firebase/app";
import "firebase";
var cidades = require('../arquivos/cidades.json');
var imovel = require('../migracoes/wilmar/cadimo.json');
var tipos = require('../migracoes/wilmar/cadcat.json');
var imagens = require('../migracoes/wilmar/cadima.json');

var clientes = require('../migracoes/wilmar/cli.json')

async function buscaImovel() {
    const firebaseConfig = {
      apiKey: 'AIzaSyCRY1RsUASByrwHaiXSzgLBa_KjcxA7HDk',
      authDomain: 'smartimob-dev-test.firebaseapp.com',
      databaseURL: 'https://smartimob-dev-test.firebaseio.com',
      projectId: 'smartimob-dev-test',
      storageBucket: 'smartimob-dev-test.appspot.com',
      messagingSenderId: '1030189605462',
      appId: '1:1030189605462:web:cb42f70fe0f892048051f7',
      measurementId: 'G-KC82QWH0ZD'
    };
    
    const app = firebase.initializeApp(firebaseConfig);
    app.firestore().settings({ experimentalForceLongPolling: true });

    return app.firestore();
}

export default class App extends Component{
    state={
        id: 0
    }
    db = () => {
        const firebaseConfig = {
            apiKey: 'AIzaSyCRY1RsUASByrwHaiXSzgLBa_KjcxA7HDk',
            authDomain: 'smartimob-dev-test.firebaseapp.com',
            databaseURL: 'https://smartimob-dev-test.firebaseio.com',
            projectId: 'smartimob-dev-test',
            storageBucket: 'smartimob-dev-test.appspot.com',
            messagingSenderId: '1030189605462',
            appId: '1:1030189605462:web:cb42f70fe0f892048051f7',
            measurementId: 'G-KC82QWH0ZD'
          };
          
          const app = firebase.initializeApp(firebaseConfig);
          app.firestore().settings({ experimentalForceLongPolling: true });

          return firebase;
    }
    async componentDidMount(){
        var bairro_disponiveis = [];
        this.MigraClientes();
        return true;
    }
    async MigraImóveis() {
        var db = this.db();
        for await( let imo of imovel.rows ) {
            let fotos = imagens.rows.filter( img => img.CODIGO === imo.CODIGO && img.VER_WEB === "Sim");
            let e = 0;
            let results = [];
            console.log(fotos)
            for await (let imagem of fotos) {
                if (e <= 30) {
                    var img = [];
                    let url = `http://www.waldemarlimaimoveis.com.br/waldemar/vista.imobi/fotos/${imagem['FILE_PATH']}`;
                    let im = await fetch(`http://localhost/test_sockets.php?imagem=${url}`).then( dados => dados.json()).catch(function(error){ return error});
                    
                    var storageRef = db.storage().ref();
                             
                    var fotoRef = storageRef.child(`empresas/O52Kg8tXkhLEdF6xBet4/${imo.CODIGO}/${imo.CODIGO}_${e}.jpeg`);
                    if (im.caminho) {
                      let snapshot = await fotoRef.putString(im.caminho, 'data_url',{contentType:'image/jpeg'})
                      results.push({
                        destaque: e === 0 ? true : false,
                        height: null,
                        id: `${imo.CODIGO}_${e}`,
                        uploaded: true,
                        width: null,
                        resized: await fotoRef.getDownloadURL(),
                        source:{uri: await fotoRef.getDownloadURL()}
                      })
                      e++;
                      console.log("foto inserida: ",e);
                    }
                }         
            }
            let caracteristica = 
            [   
                {
                    nome: imo['ABERTURAS'] ? 'ABERTURAS' : 'ABERTURAS',
                    value: imo['ABERTURAS'] ? true : false,
                },
                {
                    nome: imo['ADMINISTRADORA_COND'] ? 'ADMINISTRADORA COND' : 'ADMINISTRADORA COND',
                    value: imo['ADMINISTRADORA_COND'] ? true : false,
                },
                {
                    nome: imo['AGUA_QUENTE'] != "Nao" ? 'Água Quente' : 'Água Quente',
                    value: imo['AGUA_QUENTE'] != "Nao" ? true : false,
                },
                {
                    nome: imo['ALVENARIA'] != "Nao" ? 'Alvenaria' : 'Alvenaria',
                    value: imo['ALVENARIA'] != "Nao" ? true : false,
                },
                {
                    nome: imo['ANDAR_APTO']  ? 'ANDAR APTO' : 'ANDAR APTO',
                    value: imo['ANDAR_APTO'] ? true : false,
                },
                {
                    nome: imo['ANO_CONSTRUCAO']  ? 'ANO CONSTRUCAO' : 'ANO CONSTRUCAO',
                    value: imo['ANO_CONSTRUCAO'] ? true : false,
                },
                {
                    nome: imo['AREA_SERVICO']  ? 'AREA SERVICO' : 'AREA SERVICO',
                    value: imo['AREA_SERVICO'] ? true : false,
                },
                {
                    nome: imo['AR_CENTRAL'] != "Nao" ? 'AR_CENTRAL' : 'AR_CENTRAL',
                    value: imo['AR_CENTRAL'] != "Nao" ? true : false,
                },
                {
                    nome: imo['BANHO_AUXILIAR'] != "Nao" ? 'BANHO AUXILIAR' : 'BANHO AUXILIAR',
                    value: imo['BANHO_AUXILIAR'] != "Nao" ? true : false,
                },
                {
                    nome: imo['BANHO_SOCIAL'] != "Nao" ? 'BANHO SOCIAL' : 'BANHO SOCIAL',
                    value: imo['BANHO_SOCIAL'] != "Nao" ? true : false,
                },
                {
                    nome: imo['CHURRASQUEIRA_COLETIVA'] != "Nao" ? 'CHURRASQUEIRA COLETIVA' : 'CHURRASQUEIRA COLETIVA',
                    value: imo['CHURRASQUEIRA_COLETIVA'] != "Nao" ? true : false,
                },
                {
                    nome: imo['CIRCUITO_INTERNO_TV'] != "Nao" ? 'CIRCUITO INTERNO TV' : 'CIRCUITO INTERNO TV',
                    value: imo['CIRCUITO_INTERNO_TV'] != "Nao" ? true : false,
                },
                {
                    nome: imo['CLOSET'] != "Nao" ? 'CLOSET' : 'CLOSET',
                    value: imo['CLOSET'] != "Nao" ? true : false,
                },
                {
                    nome: imo['COPA_COZINHA'] != "Nao" ? 'COPA_COZINHA' : 'COPA_COZINHA',
                    value: imo['COPA_COZINHA'] != "Nao" ? true : false,
                },
                {
                    nome: imo['COZIHA_MONTADA'] != "Nao" ? 'COZIHA_MONTADA' : 'COZIHA_MONTADA',
                    value: imo['COZIHA_MONTADA'] != "Nao" ? true : false,
                },
                {
                    nome: imo['COZIHA'] != "Nao" ? 'COZIHA' : 'COZIHA',
                    value: imo['COZIHA'] != "Nao" ? true : false,
                },
                {
                    nome: imo['DEPOSITO'] != "Nao" ? 'DEPOSITO' : 'DEPOSITO',
                    value: imo['DEPOSITO'] != "Nao" ? true : false,
                },
                {
                    nome: imo['DESPENSA'] != "Nao" ? 'DESPENSA' : 'DESPENSA',
                    value: imo['DESPENSA'] != "Nao" ? true : false,
                },
                {
                    nome: imo['ESPACO_GOURMET'] != "Nao" ? 'ESPACO_GOURMET' : 'ESPACO_GOURMET',
                    value: imo['ESPACO_GOURMET'] != "Nao" ? true : false,
                },
                {
                    nome: imo['GABINETE'] != "Nao" ? 'GABINETE' : 'GABINETE',
                    value: imo['GABINETE'] != "Nao" ? true : false,
                },
                {
                    nome: imo['GARAGEM_COBERTA'] != "Nao" ? 'GARAGEM COBERTA' : 'GARAGEM COBERTA',
                    value: imo['GARAGEM_COBERTA'] != "Nao" ? true : false,
                },
                {
                    nome: imo['GRADEADO'] != "Nao" ? 'GRADEADO' : 'GRADEADO',
                    value: imo['GRADEADO'] != "Nao" ? true : false,
                },
                {
                    nome: imo['GRADIL'] != "Nao" ? 'GRADIL' : 'GRADIL',
                    value: imo['GRADIL'] != "Nao" ? true : false,
                },
                {
                    nome: imo['HIDRO'] != "Nao" ? 'HIDRO' : 'HIDRO',
                    value: imo['HIDRO'] != "Nao" ? true : false,
                },
                {
                    nome: imo['HIDRO'] != "Nao" ? 'HIDRO' : 'HIDRO',
                    value: imo['HIDRO'] != "Nao" ? true : false,
                }
            ];
            let caracteristica_disponiveis = 
            [   
                {
                    nome: imo['ABERTURAS'] ? 'ABERTURAS' : 'ABERTURAS',
                    id: imo['ABERTURAS'] ? 'ABERTURAS' : 'ABERTURAS',
                },
                {
                    nome: imo['ADMINISTRADORA_COND'] ? 'ADMINISTRADORA COND' : 'ADMINISTRADORA COND',
                    id: imo['ADMINISTRADORA_COND'] ? 'ADMINISTRADORA COND' : 'ADMINISTRADORA COND',
                },
                {
                    nome: imo['AGUA_QUENTE'] != "Nao" ? 'Água Quente' : 'Água Quente',
                    id: imo['AGUA_QUENTE'] != "Nao" ? 'Água Quente' : 'Água Quente'
                },
                {
                    nome: imo['ALVENARIA'] != "Nao" ? 'Alvenaria' : 'Alvenaria',
                    id: imo['ALVENARIA'] != "Nao" ? 'Alvenaria' : 'Alvenaria',
                },
                {
                    nome: imo['ANDAR_APTO']  ? 'ANDAR APTO' : 'ANDAR APTO',
                    id: imo['ANDAR_APTO']  ? 'ANDAR APTO' : 'ANDAR APTO',
                },
                {
                    nome: imo['ANO_CONSTRUCAO']  ? 'ANO CONSTRUCAO' : 'ANO CONSTRUCAO',
                    id: imo['ANO_CONSTRUCAO']  ? 'ANO CONSTRUCAO' : 'ANO CONSTRUCAO',
                },
                {
                    nome: imo['AREA_SERVICO']  ? 'AREA SERVICO' : 'AREA SERVICO',
                    id: imo['AREA_SERVICO']  ? 'AREA SERVICO' : 'AREA SERVICO',
                },
                {
                    nome: imo['AR_CENTRAL'] != "Nao" ? 'AR_CENTRAL' : 'AR_CENTRAL',
                    id: imo['AR_CENTRAL'] != "Nao" ? 'AR_CENTRAL' : 'AR_CENTRAL',
                },
                {
                    nome: imo['BANHO_AUXILIAR'] != "Nao" ? 'BANHO AUXILIAR' : 'BANHO AUXILIAR',
                    id: imo['BANHO_AUXILIAR'] != "Nao" ? 'BANHO_AUXILIAR' : 'BANHO AUXILIAR',
                },
                {
                    nome: imo['BANHO_SOCIAL'] != "Nao" ? 'BANHO SOCIAL' : 'BANHO SOCIAL',
                    id: imo['BANHO_SOCIAL'] != "Nao" ? 'BANHO SOCIAL' : 'BANHO SOCIAL'
                },
                {
                    nome: imo['CHURRASQUEIRA_COLETIVA'] != "Nao" ? 'CHURRASQUEIRA COLETIVA' : 'CHURRASQUEIRA COLETIVA',
                    id: imo['CHURRASQUEIRA_COLETIVA'] != "Nao" ? 'CHURRASQUEIRA COLETIVA' : 'CHURRASQUEIRA COLETIVA',
                },
                {
                    nome: imo['CIRCUITO_INTERNO_TV'] != "Nao" ? 'CIRCUITO INTERNO TV' : 'CIRCUITO INTERNO TV',
                    id: imo['CIRCUITO_INTERNO_TV'] != "Nao" ? 'CIRCUITO INTERNO TV' : 'CIRCUITO INTERNO TV'
                },
                {
                    nome: imo['CLOSET'] != "Nao" ? 'CLOSET' : 'CLOSET',
                    id: imo['CLOSET'] != "Nao" ? 'CLOSET' : 'CLOSET',
                },
                {
                    nome: imo['COPA_COZINHA'] != "Nao" ? 'COPA_COZINHA' : 'COPA_COZINHA',
                    id: imo['COPA_COZINHA'] != "Nao" ? 'COPA_COZINHA' : 'COPA_COZINHA',
                },
                {
                    nome: imo['COZIHA_MONTADA'] != "Nao" ? 'COZIHA_MONTADA' : 'COZIHA_MONTADA',
                    id: imo['COZIHA_MONTADA'] != "Nao" ? 'COZIHA_MONTADA' : 'COZIHA_MONTADA',
                },
                {
                    nome: imo['COZIHA'] != "Nao" ? 'COZIHA' : 'COZIHA',
                    id: imo['COZIHA'] != "Nao" ? 'COZIHA' : 'COZIHA',
                },
                {
                    nome: imo['DEPOSITO'] != "Nao" ? 'DEPOSITO' : 'DEPOSITO',
                    id: imo['DEPOSITO'] != "Nao" ? 'DEPOSITO' : 'DEPOSITO',
                },
                {
                    nome: imo['DESPENSA'] != "Nao" ? 'DESPENSA' : 'DESPENSA',
                    id: imo['DESPENSA'] != "Nao" ? 'DESPENSA' : 'DESPENSA',
                },
                {
                    nome: imo['ESPACO_GOURMET'] != "Nao" ? 'ESPACO_GOURMET' : 'ESPACO_GOURMET',
                    id: imo['ESPACO_GOURMET'] != "Nao" ? 'ESPACO_GOURMET' : 'ESPACO_GOURMET',
                },
                {
                    nome: imo['GABINETE'] != "Nao" ? 'GABINETE' : 'GABINETE',
                    id: imo['GABINETE'] != "Nao" ? 'GABINETE' : 'GABINETE',
                },
                {
                    nome: imo['GARAGEM'] != "Nao" ? 'GARAGEM' : 'GARAGEM',
                    id: imo['GARAGEM'] != "Nao" ? 'GARAGEM' : 'GARAGEM',
                },
                {
                    nome: imo['GARAGEM_COBERTA'] != "Nao" ? 'GARAGEM COBERTA' : 'GARAGEM COBERTA',
                    id: imo['GARAGEM_COBERTA'] != "Nao" ? 'GARAGEM COBERTA' : 'GARAGEM COBERTA',
                },
                {
                    nome: imo['GRADEADO'] != "Nao" ? 'GRADEADO' : 'GRADEADO',
                    id: imo['GRADEADO'] != "Nao" ? 'GRADEADO' : 'GRADEADO',
                },
                {
                    nome: imo['GUARITA'] != "Nao" ? 'GUARITA' : 'GUARITA',
                    id: imo['GUARITA'] != "Nao" ? 'GUARITA' : 'GUARITA',
                },
                {
                    nome: imo['HIDRO'] != "Nao" ? 'HIDRO' : 'HIDRO',
                    id: imo['HIDRO'] != "Nao" ? 'HIDRO' : 'HIDRO',
                }
            ]
            let id_estado = cidades.filter( city => city.municipio.microrregiao.mesorregiao.UF.sigla == imo['UF']).length > 0 ? cidades.filter( city => city.municipio.microrregiao.mesorregiao.UF.sigla == imo['UF'])[0].municipio.microrregiao.mesorregiao.UF.id : "";
            let cidade = cidades.filter( city => city.municipio.nome.includes(imo['CIDADE']) && city.municipio.microrregiao.mesorregiao.UF.sigla == imo['UF']).length > 0 ? cidades.filter( city => city.municipio.nome.includes(imo['CIDADE']) && city.municipio.microrregiao.mesorregiao.UF.sigla == imo['UF'])[0].municipio.id : "";
            if (imo.CIDADE === "Embu Das Artes") {
                cidade = 3515004;
            }
            let template = {
                caracteristicas: [],
                banheiros: '',
                preço_condominio: 0,
                fotos: [],
                fotos_thumbnail: [],
                CEP_confirmado: true,
                CEP_error: false,
                excluido: false,
                agenciador_id: "FreOoUECyIWDRUuzsHkhB55ipJw1",
                codigo: '',
                db_id: '',
                aut_venda: [],
                aut_visita: [],
                cadastrador_id: "",
                campos_personalizados_values: [],
                created_at: new Date(),
                proprietario_id: '',
                bairro: '',
                rua: '',
                número: '',
                estado: {
                id: '',
                nome: '',
                value: ''
                },
                CEP: '',
                cidade: {
                id: '',
                nome: '',
                value: ''
                },
                exibirEndereco: true,
                vagas: '',
                suítes: '',
                dormitórios: '',
                tipo: '',
                aceita_permuta: false,
                mostrar_permuta_no_site: false,
                venda_exibir_valor_no_site: true,
                permuta_desc: "",
                area_total: '',
                area_construída: "",
                area_privativa: "",
                area_útil: "",
                preço_venda: '',
                preço_venda_desconto: 0,
                preço_locação: 0,
                preço_locação_desconto: 0,
                venda_autorizacao: false,
                venda_financiamento: false,
                locação_exibir_valor_no_site: false,
                locação_autorizacao: false,
                bloqueio_fotos: false,
                locação: false,
                venda: true,
                IPTU: 0,
                titulo: "",
                descrição: "",
                desc_interna: "",
                video_youtube: "",
                palavras_chaves: "",
                link_tour_virtual: "",
                inativo: false,
                destaque: ""
            }
            template = {
                ...template,
                area_total: imo.AREA_TOTAL,
                bairro: imo.BAIRRO,
                banheiros: imo.BANHEIRO_SOCIAL,
                CEP: imo.CEP,
                cidade: imo.CIDADE,
                caracteristicas: caracteristica,
                codigo: imo.CODIGO,
                complemento: imo.COMP_ENDERECO,
                created_at: new Date(imo.DATA),
                update_at: new Date(imo.DATA_ATUALIZACAO),
                dormitórios: imo.DORMITORIO,
                rua: imo.ENDERECO,
                vagas: imo.VAGAS,
                lat: imo.GMAPS_LAT,
                long: imo.GMAPS_LNG,
                número: imo.NUM_ENDERECO,
                descrição: imo.OBS,
                titulo: imo.TITULO_SITE,
                inativo: imo.VER_WEB && imo.VER_WEB != "Nao" ? true : false,
                excluido: imo.DATA_INATIVACAO_IMOVEL ? true : false,
                preço_condominio: imo.VLR_CONDOMINIO,
                IPTU: imo.VLR_IPTU,
                preço_locação: imo.VLR_ALUGUEL,
                preço_venda: imo.VLR_VENDA,
                venda: imo.VLR_ALUGUEL ? true : false,
                locação: imo.VLR_MENSAL ? true : false,
                area_privativa: imo.AREA_PRIVATIVA,
                area_construída: imo.AREA_CONSTRUIDA,
                descrição: imo.DESCRICAO,
                tipo: tipos.rows.filter( t => t.CODIGO === imo.CODIGO_CT)[0].CATEGORIA,
                proprietario_id: imo.CODIGO_C,
                fotos: results,
                db_id: imo.CODIGO,
                estado: {
                    id: id_estado,
                    nome: cidades.filter( city => city.municipio.microrregiao.mesorregiao.UF.sigla == imo['UF']).length > 0 ? cidades.filter( city => city.municipio.microrregiao.mesorregiao.UF.sigla == imo['UF'])[0].municipio.microrregiao.mesorregiao.UF.nome : "",
                    value: id_estado
                },
                cidade: {
                    id: cidade,
                    nome: imo.CIDADE,
                    value: cidade
                }
            }
            if ( bairro_disponiveis.filter( bairro => bairro.bairro == imo.BAIRRO).length == 0 && imo.BAIRRO) {
                bairro_disponiveis.push({
                  bairro: imo.BAIRRO,
                  cidade: cidade,
                  estado: id_estado,
                })
            }
            db.firestore().doc(`empresas/O52Kg8tXkhLEdF6xBet4/imoveis/${imo.CODIGO}`).set({...template});
            db.firestore().doc(`empresas/O52Kg8tXkhLEdF6xBet4`).update({bairros_disponiveis: bairro_disponiveis,caracteristicas_disponiveis: caracteristica_disponiveis})
            console.log("inserido: ",imo.CODIGO)
            // if (fotos.length > 0)
            //     return true;
        }
    }
    async MigraClientes() {
        var db = this.db();
        for await (let cli of clientes.rows) {
            let cliente = {
                foto: null,
                nome: '',
                email: null,
                telefone: null,
                CPF: null,
                FGTS: null,
                excluido: false,
                dependentes: null,
                entrada: 1,
                imoveis_cadastrados: [],
                imoveis_visitados: [],
                possui_emprestimo: false,
                funil_etapa_venda: 0,
                renda: null,
                proprietario: false,
                status: 'Cadastrado pela landpage',
                troca: false,
                DDD: 1,
                utilizar_financiamento: false,
                created_at: new Date(),
                edited_at: new Date()
            }
            if (cli.EMAIL_R) {
                cliente = {
                    ...cliente,
                    nome: cli.NOME,
                    DDD: cli.CELULAR.substr(0,3),
                    telefone: cli.CELULAR.substr(3,11),
                    db_id: cli.CODIGO_C,
                    email: cli.EMAIL_R,
                    created_at: new Date(cli.DATA)
                }
                console.log(cliente)
                db.firestore().doc(`empresas/O52Kg8tXkhLEdF6xBet4/clientes/${cli.CODIGO_C}`).set({...cliente})
            }            
        }
    }
    render() {
        return(
            <div className="migrar">
                id: 123
            </div>
        )
    }
}