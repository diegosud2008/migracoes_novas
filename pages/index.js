import Head from 'next/head'
import firebase from "firebase/app";
import "firebase";
var passione = require('../arquivos/passione.json');
var cidades = require('../arquivos/cidades.json');
export default function Home() {
  async function buscaImovel() {
    const firebaseConfig = {
      apiKey: 'AIzaSyCRY1RsUASByrwHaiXSzgLBa_KjcxA7HDk',
      authDomain: 'smartimob-dev-test.firebaseapp.com',
      databaseURL: 'https://smartimob-dev-test.firebaseio.com',
      projectId: 'smartimob-dev-test',
      storageBucket: 'smartimob-dev-test.appspot.com',
      messagingSenderId: '1030189605462',
      appId: '1:1030189605462:web:cb42f70fe0f892048051f7',
      measurementId: 'G-KC82QWH0ZD'
    };
    
    const app = firebase.initializeApp(firebaseConfig);
    app.firestore().settings({ experimentalForceLongPolling: true });
  
    var imovel = [];
    var imagem = [];
    var caracteristicas = [];
    var regioes = [
      {
        id: 35,
        sigla: "SP",
        nome: "São Paulo",
        regiao: {
          id: 3,
          sigla: "SE",
          nome: "Sudeste"
        }
      },
      {
        id: 31,
        sigla: "MG",
        nome: "Minas Gerais",
        regiao: {
          id: 3,
          sigla: "SE",
          nome: "Sudeste"
        }
      }
    ]
    var bairro_disponiveis = [];
    var caracteristicas_disponiveis = [];
    for await ( let imo of passione.Planilha1 ) {
        if (imovel.filter( i => i.Ref.includes(imo.Ref)).length === 0) {
            caracteristicas = [];
            if (imo.Feature){
              caracteristicas.push({nome: imo.Feature,value: true});
              if ((caracteristicas_disponiveis.filter( cr => cr.nome == imo.Feature) == 0) && imo.Feature) {
                caracteristicas_disponiveis.push({
                  id: imo.Feature,
                  nome: imo.Feature
                })
              }
            }
            imovel.push({...imo,caracteristicas,foto: imo.MediaItem});
            imagem = [];
        } else {
            if (imo.MediaItem){
                imagem.push(imo.MediaItem);
            }
            if (imo.Feature){
              caracteristicas.push({nome: imo.Feature,value: true});
            }
            var index = imovel.findIndex( im => im.Ref == imo.Ref);
            imovel[index] = {...imovel[index],caracteristicas,foto: imagem};
        }
    }
    for await (let imo of imovel ) {
      let template = {
          caracteristicas: [],
          banheiros: '',
          preço_condominio: 0,
          fotos: [],
          fotos_thumbnail: [],
          CEP_confirmado: true,
          CEP_error: false,
          excluido: false,
          agenciador_id: "",
          codigo: '',
          db_id: '',
          aut_venda: [],
          aut_visita: [],
          cadastrador_id: "",
          campos_personalizados_values: [],
          created_at: new Date(),
          proprietario_id: '',
          bairro: '',
          rua: '',
          número: '',
          estado: {
          id: '',
          nome: '',
          value: ''
          },
          CEP: '',
          cidade: {
          id: '',
          nome: '',
          value: ''
          },
          exibirEndereco: true,
          vagas: '',
          suítes: '',
          dormitórios: '',
          tipo: '',
          aceita_permuta: false,
          mostrar_permuta_no_site: false,
          venda_exibir_valor_no_site: true,
          permuta_desc: "",
          area_total: '',
          area_construída: "",
          area_privativa: "",
          area_útil: "",
          preço_venda: '',
          preço_venda_desconto: 0,
          preço_locação: 0,
          preço_locação_desconto: 0,
          venda_autorizacao: false,
          venda_financiamento: false,
          locação_exibir_valor_no_site: false,
          locação_autorizacao: false,
          bloqueio_fotos: false,
          locação: false,
          venda: true,
          IPTU: 0,
          titulo: "",
          descrição: '',
          desc_interna: "",
          video_youtube: "",
          palavras_chaves: "",
          link_tour_virtual: "",
          inativo: false,
          destaque: ""
      }
      var results = []
      var e = 0;
      let imo_import = await firebase.firestore().doc(`empresas/EvZLQ1TKsPUabU95tIfk/imoveis/${imo.Ref}`).get();
      imo_import = await imo_import.data();
      if (imo.foto && imo.foto.length > 0 && !imo_import) {
          for await (let imagem of imo.foto) {
              var img = [];
              
              let im = await fetch(`http://localhost/test_sockets.php?imagem=${imagem}`).then( dados => dados.json()).catch(function(error){ return error});
              
              var storageRef = firebase.storage().ref();
                       
              var fotoRef = storageRef.child(`empresas/EvZLQ1TKsPUabU95tIfk/${imo.Ref}/${imo.Ref}_${e}.jpeg`);
              if (im.caminho) {
                let snapshot = await fotoRef.putString(im.caminho, 'data_url',{contentType:'image/jpeg'})
                results.push({
                  destaque: e === 0 ? true : false,
                  height: null,
                  id: `${imo.Ref}_${e}`,
                  uploaded: true,
                  width: null,
                  resized: await fotoRef.getDownloadURL(),
                  source:{uri: await fotoRef.getDownloadURL()}
                })
                e++;
                console.log("foto inserida: ",e); 
              }         
          }
      }
      // console.log(regioes.filter( estados => estados.sigla == imo.State10).length > 0 ? regioes.filter( estados => estados.sigla == imo.State10)[0].id : "");
      // return true;
      template = {
        ...template,
        tipo: imo.Type ? imo.Type : "",
        fotos: results,
        codigo: imo.Ref,
        inativo: imo.Released == "FALSO" ? false : true,
        dormitórios: imo.Rooms ? imo.Rooms : 0,
        suítes: imo.Suites ? imo.Suites : 0,
        banheiros: imo.Restrooms ? imo.Restrooms : 0,
        vagas: imo.Parking ? imo.Parking : 0,
        area_útil: imo.UtilArea ? imo.UtilArea : "",
        area_total: imo.TotalArea ? imo.TotalArea : "",
        descrição: imo.info ? imo.info : "",
        created_at: imo.LastUpdateDate ? imo.LastUpdateDate : new Date(),
        edited_at: imo.LastUpdateDate ? imo.LastUpdateDate : new Date(),
        preço_venda: imo.SellPrice ? imo.SellPrice : "",
        venda: imo.SellPrice > 0 ? true : false,
        locação: imo.RentPrice > 0 ? true : false,
        preço_condominio: imo.CondominiumPrice ? imo.CondominiumPrice : "",
        caracteristicas: imo.caracteristicas.length > 0 ? imo.caracteristicas : [],
        estado: {
          id: regioes.filter( estados => estados.sigla == imo.State10).length > 0 ? regioes.filter( estados => estados.sigla == imo.State10)[0].id : "",
          nome: regioes.filter( estados => estados.sigla == imo.State10).length > 0 ? regioes.filter( estados => estados.sigla == imo.State10)[0].nome : "",
          value: regioes.filter( estados => estados.sigla == imo.State10).length > 0 ? regioes.filter( estados => estados.sigla == imo.State10)[0].id : "",
        },
        cidade: {
          id: cidades.filter( city => city.nome == imo.City11 && city.municipio.microrregiao.mesorregiao.UF.sigla == imo.State10).length > 0 ? cidades.filter( city => city.nome == imo.City11 && city.municipio.microrregiao.mesorregiao.UF.sigla == imo.State10)[0].municipio.id : "",
          nome: imo.City11,
          value: cidades.filter( city => city.nome == imo.City11 && city.municipio.microrregiao.mesorregiao.UF.sigla == imo.State10).length > 0 ? cidades.filter( city => city.nome == imo.City11 && city.municipio.microrregiao.mesorregiao.UF.sigla == imo.State10)[0].municipio.id : "",
        },
        bairro: imo.Neighborhood12 ? imo.Neighborhood12 : "",
        rua: imo.Address13 ? imo.Address13 : "",
        número: imo.Number14 ? imo.Number14 : "",
        CEP: imo.PostalCode15 ? imo.PostalCode15 : "",
        lat: imo.Latitude16 ? imo.Latitude16 : "",
        long: imo.Longitude17 ? imo.Longitude17 : "",
        complemento: imo.Name18 && imo.Tower ? "nome: "+imo.Name18+", bloco: "+imo.Tower : imo.Name18 ? "nome: "+imo.Name18 : "",
        preço_locação: imo.RentPrice ? imo.RentPrice : ""
      }
      if ( bairro_disponiveis.filter( bairro => bairro.bairro == imo.Neighborhood12).length == 0 && imo.Neighborhood12) {
        bairro_disponiveis.push({
          bairro: imo.Neighborhood12,
          cidade: cidades.filter( city => city.nome == imo.City11 && city.municipio.microrregiao.mesorregiao.UF.sigla == imo.State10).length > 0 ? cidades.filter( city => city.nome == imo.City11 && city.municipio.microrregiao.mesorregiao.UF.sigla == imo.State10)[0].municipio.id : "",
          estado: regioes.filter( estados => estados.sigla == imo.State10).length > 0 ? regioes.filter( estados => estados.sigla == imo.State10)[0].id : "",
        })
      }
      
      
      if(!imo_import)
        var imovel = firebase.firestore().doc(`empresas/EvZLQ1TKsPUabU95tIfk/imoveis/${imo.Ref}`).set({...template});
      firebase.firestore().doc(`empresas/EvZLQ1TKsPUabU95tIfk`).update({bairros_disponiveis: bairro_disponiveis,caracteristicas_disponiveis: caracteristicas_disponiveis})
      console.log("inserido: ",imo.Ref)
      // console.clear();
    }
    console.log(bairro_disponiveis,caracteristicas_disponiveis)
  }
  buscaImovel();
  return (
    <div>
      tset
    </div>
  )
}
